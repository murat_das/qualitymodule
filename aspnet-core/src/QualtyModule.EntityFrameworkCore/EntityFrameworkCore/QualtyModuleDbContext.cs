﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using QualtyModule.Authorization.Roles;
using QualtyModule.Authorization.Users;
using QualtyModule.Domain.Configurations;
using QualtyModule.Domain.Entities;
using QualtyModule.MultiTenancy;

namespace QualtyModule.EntityFrameworkCore
{
    public class QualtyModuleDbContext : AbpZeroDbContext<Tenant, Role, User, QualtyModuleDbContext>
    {
        /* Define a DbSet for each entity of the application */
        #region DBSet
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductControl> ProductControls { get; set; }
        public DbSet<ProductControlTest> ProductControlTests { get; set; }
        public DbSet<ProductScrap> ProductScraps { get; set; }
        public DbSet<ProductTest> ProductTests { get; set; }
        public DbSet<Rule> Rules { get; set; }
        public DbSet<Scrap> Scraps { get; set; }
        public DbSet<ScrapType> ScrapTypes { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestParameter> TestParameters { get; set; }
        public DbSet<TestResult> TestResults { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<ValueType> ValueTypes { get; set; }

        #endregion DBSet

        public QualtyModuleDbContext(DbContextOptions<QualtyModuleDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ApplyConfiguration(new ProductConfiguration())
                .ApplyConfiguration(new ProductControlConfiguration())
                .ApplyConfiguration(new ProductControlTestConfiguration())
                .ApplyConfiguration(new ProductScrapConfiguration())
                .ApplyConfiguration(new ProductTestConfiguration())
                .ApplyConfiguration(new RuleConfiguration())
                .ApplyConfiguration(new ScrapConfiguration())
                .ApplyConfiguration(new TestConfiguration())
                .ApplyConfiguration(new TestParameterConfiguration())
                .ApplyConfiguration(new TestResultConfiguration())
                .ApplyConfiguration(new UnitConfiguration())
                .ApplyConfiguration(new ValueTypeConfiguration());

        }
    }
}
