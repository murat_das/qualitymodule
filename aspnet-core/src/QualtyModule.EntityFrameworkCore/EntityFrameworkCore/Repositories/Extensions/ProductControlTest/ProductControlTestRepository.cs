﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class ProductControlTestRepository:QualtyModuleRepositoryBase<ProductControlTest,int>,IProductControlTestRepository
    {
        public ProductControlTestRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}