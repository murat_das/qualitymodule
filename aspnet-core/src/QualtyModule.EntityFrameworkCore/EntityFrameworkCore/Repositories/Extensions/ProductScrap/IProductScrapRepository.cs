﻿using Abp.Domain.Repositories;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public interface IProductScrapRepository:IRepository<ProductScrap,int>
    {
        
    }
}