﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class ProductScrapRepository:QualtyModuleRepositoryBase<ProductScrap,int>,IProductScrapRepository
    {
        public ProductScrapRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}