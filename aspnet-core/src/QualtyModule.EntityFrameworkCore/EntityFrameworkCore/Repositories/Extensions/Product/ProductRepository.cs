﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class ProductRepository:QualtyModuleRepositoryBase<Product,int>,IProductRepository
    {
        public ProductRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}