﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class ValueTypeRepository:QualtyModuleRepositoryBase<ValueType,int>,IValueTypeRepository
    {
        public ValueTypeRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}