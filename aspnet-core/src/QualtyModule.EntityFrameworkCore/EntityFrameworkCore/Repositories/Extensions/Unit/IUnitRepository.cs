﻿using Abp.Domain.Repositories;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public interface IUnitRepository : IRepository<Unit, int>
    {

    }
}