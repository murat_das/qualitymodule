﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class UnitRepository : QualtyModuleRepositoryBase<Unit, int>, IUnitRepository
    {
        public UnitRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}