﻿using Abp.Domain.Repositories;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public interface IProductControlRepository:IRepository<ProductControl,int>
    {
        
    }
}