﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class ScrapTypeRepository:QualtyModuleRepositoryBase<ScrapType,int>,IScrapTypeRepository
    {
        public ScrapTypeRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}