﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class ProductTestRepository:QualtyModuleRepositoryBase<ProductTest,int>,IProductTestRepository
    {
        public ProductTestRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}