﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class TestRepository:QualtyModuleRepositoryBase<Test,int>,ITestRepository
    {
        public TestRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}