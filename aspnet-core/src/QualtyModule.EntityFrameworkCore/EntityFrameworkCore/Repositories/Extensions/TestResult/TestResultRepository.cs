﻿using Abp.EntityFrameworkCore;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories
{
    public class TestResultRepository:QualtyModuleRepositoryBase<TestResult,int>,ITestResultRepository
    {
        public TestResultRepository(IDbContextProvider<QualtyModuleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}