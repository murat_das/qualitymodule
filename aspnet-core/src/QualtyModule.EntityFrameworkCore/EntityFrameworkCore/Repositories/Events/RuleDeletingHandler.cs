﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class RuleDeletingHandler : IEventHandler<EntityDeletingEventData<Rule>>, ITransientDependency
    {
        private readonly ITestParameterRepository _testParameterRepository;

        public RuleDeletingHandler(ITestParameterRepository testParameterRepository)
        {
            _testParameterRepository = testParameterRepository;
        }

        public void HandleEvent(EntityDeletingEventData<Rule> eventData)
        {
            var rule = eventData.Entity;

            foreach (var testParameter in rule.TestParameters)
            {
                _testParameterRepository.Delete(testParameter);
            }
        }
    }
}