﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class ScrapTypeDeletingHandler : IEventHandler<EntityDeletingEventData<ScrapType>>, ITransientDependency
    {
        private readonly IProductScrapRepository _productScrapRepository;

        public ScrapTypeDeletingHandler(IProductScrapRepository productScrapRepository)
        {
            _productScrapRepository = productScrapRepository;
        }

        public void HandleEvent(EntityDeletingEventData<ScrapType> eventData)
        {
            var scrapType = eventData.Entity;

            foreach (var productScrap in scrapType.ProductScraps)
            {
                _productScrapRepository.Delete(productScrap);
            }
        }
    }
}