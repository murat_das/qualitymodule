﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class ProductTestDeletingHandler : IEventHandler<EntityDeletingEventData<ProductTest>>, ITransientDependency
    {
        private readonly ITestParameterRepository _testParameterRepository;

        public ProductTestDeletingHandler(ITestParameterRepository testParameterRepository)
        {
            _testParameterRepository = testParameterRepository;
        }

        public void HandleEvent(EntityDeletingEventData<ProductTest> eventData)
        {
            var productTest = eventData.Entity;

            foreach (var testParameter in productTest.TestParameters)
            {
                _testParameterRepository.Delete(testParameter);
            }
        }
    }
}