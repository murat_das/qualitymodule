﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class ProductControlDeletingHandler : IEventHandler<EntityDeletingEventData<ProductControl>>, ITransientDependency
    {
        private readonly IScrapRepository _scrapRepository;
        private readonly IProductControlTestRepository _productControlTestRepository;
        private readonly ITestResultRepository _testResultRepository;

        public ProductControlDeletingHandler(IScrapRepository scrapRepository, IProductControlTestRepository productControlTestRepository, ITestResultRepository testResultRepository)
        {
            _scrapRepository = scrapRepository;
            _productControlTestRepository = productControlTestRepository;
            _testResultRepository = testResultRepository;
        }

        public void HandleEvent(EntityDeletingEventData<ProductControl> eventData)
        {
            var productControl = eventData.Entity;

            foreach (var scrap in productControl.Scraps)
            {
                _scrapRepository.Delete(scrap);
            }

            foreach (var testResult in productControl.TestResults)
            {
                _testResultRepository.Delete(testResult);
            }

            foreach (var productControlTest in productControl.ProductControlTests)
            {
                _productControlTestRepository.Delete(productControlTest);
            }

        }
    }
}