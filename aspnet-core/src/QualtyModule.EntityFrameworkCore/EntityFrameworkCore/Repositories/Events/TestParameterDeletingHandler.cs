﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class TestParameterDeletingHandler:IEventHandler<EntityDeletingEventData<TestParameter>>,ITransientDependency
    {
        private readonly ITestResultRepository _testResultRepository;

        public TestParameterDeletingHandler(ITestResultRepository testResultRepository)
        {
            _testResultRepository = testResultRepository;
        }

        public void HandleEvent(EntityDeletingEventData<TestParameter> eventData)
        {
            var testParameter = eventData.Entity;

            foreach (var testResult in testParameter.TestResults)
            {
                _testResultRepository.Delete(testResult);
            }
        }
    }
}