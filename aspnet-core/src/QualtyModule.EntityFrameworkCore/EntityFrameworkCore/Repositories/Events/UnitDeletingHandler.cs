﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class UnitDeletingHandler:IEventHandler<EntityDeletingEventData<Unit>>,ITransientDependency
    {
        private readonly ITestParameterRepository _testParameterRepository;

        public UnitDeletingHandler(ITestParameterRepository testParameterRepository)
        {
            _testParameterRepository = testParameterRepository;
        }

        public void HandleEvent(EntityDeletingEventData<Unit> eventData)
        {
            var unit = eventData.Entity;

            foreach (var testParameter in unit.TestParameters)
            {
                _testParameterRepository.Delete(testParameter);
            }
        }
    }
}