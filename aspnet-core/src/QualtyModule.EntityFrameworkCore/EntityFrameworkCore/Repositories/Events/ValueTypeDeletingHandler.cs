﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class ValueTypeDeletingHandler : IEventHandler<EntityDeletingEventData<ValueType>>, ITransientDependency
    {
        private readonly ITestParameterRepository _testParameterRepository;

        public ValueTypeDeletingHandler(ITestParameterRepository testParameterRepository)
        {
            _testParameterRepository = testParameterRepository;
        }

        public void HandleEvent(EntityDeletingEventData<ValueType> eventData)
        {
            var valueType = eventData.Entity;

            foreach (var testParameter in valueType.TestParameters)
            {
                _testParameterRepository.Delete(testParameter);
            }
        }
    }
}