﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class TestDeletingHandler:IEventHandler<EntityDeletingEventData<Test>>,ITransientDependency
    {
        private readonly IProductTestRepository _productTestRepository;
        private readonly IProductControlTestRepository _productControlTestRepository;


        public TestDeletingHandler(IProductTestRepository productTestRepository, IProductControlTestRepository productControlTestRepository)
        {
            _productTestRepository = productTestRepository;
            _productControlTestRepository = productControlTestRepository;
        }

        public void HandleEvent(EntityDeletingEventData<Test> eventData)
        {
            var test = eventData.Entity;

            foreach (var productTest in test.ProductTests)
            {
                _productTestRepository.Delete(productTest);
            }

            foreach (var productControlTest in test.ProductControlTests)
            {
                _productControlTestRepository.Delete(productControlTest);
            }
        }
    }
}