﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class ProductDeletingHandler : IEventHandler<EntityDeletingEventData<Product>>, ITransientDependency
    {
        private readonly IProductScrapRepository _productScrapRepository;
        private readonly IProductControlRepository _productControlRepository;
        private readonly IProductTestRepository _productTestRepository;

        public ProductDeletingHandler(IProductScrapRepository productScrapRepository, IProductControlRepository productControlRepository, IProductTestRepository productTestRepository)
        {
            _productScrapRepository = productScrapRepository;
            _productControlRepository = productControlRepository;
            _productTestRepository = productTestRepository;
        }

        public void HandleEvent(EntityDeletingEventData<Product> eventData)
        {
            var product = eventData.Entity;

            foreach (var productScrap in product.ProductScraps)
            {
                _productScrapRepository.Delete(productScrap);
            }

            foreach (var productControl in product.ProductControls)
            {
                _productControlRepository.Delete(productControl);
            }

            foreach (var productTest in product.ProductTests)
            {
                _productTestRepository.Delete(productTest);
            }
        }
    }
}