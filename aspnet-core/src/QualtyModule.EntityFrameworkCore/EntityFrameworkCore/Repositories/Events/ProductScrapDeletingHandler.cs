﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using QualtyModule.Domain.Entities;

namespace QualtyModule.EntityFrameworkCore.Repositories.Events
{
    public class ProductScrapDeletingHandler:IEventHandler<EntityDeletingEventData<ProductScrap>>,ITransientDependency
    {
        private readonly IScrapRepository _scrapRepository;

        public ProductScrapDeletingHandler(IScrapRepository scrapRepository)
        {
            _scrapRepository = scrapRepository;
        }

        public void HandleEvent(EntityDeletingEventData<ProductScrap> eventData)
        {
            var productScrap = eventData.Entity;

            foreach (var scrap in productScrap.Scraps)
            {
                _scrapRepository.Delete(scrap);
            }
        }
    }
}