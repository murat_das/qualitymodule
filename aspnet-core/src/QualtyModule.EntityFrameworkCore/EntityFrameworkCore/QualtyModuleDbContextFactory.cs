﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using QualtyModule.Configuration;
using QualtyModule.Web;

namespace QualtyModule.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class QualtyModuleDbContextFactory : IDesignTimeDbContextFactory<QualtyModuleDbContext>
    {
        public QualtyModuleDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<QualtyModuleDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
            builder.UseLazyLoadingProxies();

            QualtyModuleDbContextConfigurer.Configure(builder, configuration.GetConnectionString(QualtyModuleConsts.ConnectionStringName));

            return new QualtyModuleDbContext(builder.Options);
        }
    }
}
