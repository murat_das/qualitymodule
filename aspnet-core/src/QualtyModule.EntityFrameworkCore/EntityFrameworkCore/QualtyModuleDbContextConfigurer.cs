using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace QualtyModule.EntityFrameworkCore
{
    public static class QualtyModuleDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<QualtyModuleDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<QualtyModuleDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
