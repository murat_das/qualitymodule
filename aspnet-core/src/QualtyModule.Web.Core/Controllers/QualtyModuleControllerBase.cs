using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace QualtyModule.Controllers
{
    public abstract class QualtyModuleControllerBase: AbpController
    {
        protected QualtyModuleControllerBase()
        {
            LocalizationSourceName = QualtyModuleConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
