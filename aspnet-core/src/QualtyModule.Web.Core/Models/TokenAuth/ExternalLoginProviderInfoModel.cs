﻿using Abp.AutoMapper;
using QualtyModule.Authentication.External;

namespace QualtyModule.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
