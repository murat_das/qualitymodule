﻿namespace QualtyModule
{
    public class QualtyModuleConsts
    {
        public const string LocalizationSourceName = "QualtyModule";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
