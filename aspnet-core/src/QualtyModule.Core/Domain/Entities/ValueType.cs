﻿using System.Collections.Generic;
using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    public class ValueType : FullAuditedEntity<int>
    {
        #region Constructor
        public ValueType()
        {
            TestParameters = new HashSet<TestParameter>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public virtual ICollection<TestParameter> TestParameters { get; set; }

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}