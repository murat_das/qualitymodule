﻿using System;
using Abp.Domain.Entities.Auditing;
using QualtyModule.Authorization.Users;

namespace QualtyModule.Domain.Entities
{
    public class TestResult : FullAuditedEntity<int>
    {
        #region Constructor
        public TestResult()
        {

        }
        #endregion Constructor

        #region Properties
        public string Value { get; set; }
        public bool IsOK { get; set; }
        public int? IsOKUpdatedUserId { get; set; }
        public DateTime IsOKUpdatedDate { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations

        //public virtual User User { get; set; }
        public int? TestParameterId { get; set; }
        public virtual TestParameter TestParameter { get; set; }
        public int? ProductControlId { get; set; }
        public virtual ProductControl ProductControl { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}