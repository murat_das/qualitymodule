﻿using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    public class ProductControlTest : FullAuditedEntity<int>
    {
        #region Constructor
        public ProductControlTest()
        {

        }
        #endregion Constructor

        #region Properties
        public bool IsOK { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int? ProductControlId { get; set; }
        public virtual ProductControl ProductControl { get; set; }
        public int? TestId { get; set; }
        public virtual Test Test { get; set; }

        #endregion One To One Relations

        #region One To Many Relations


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}