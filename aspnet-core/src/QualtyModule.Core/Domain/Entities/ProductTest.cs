﻿using System.Collections.Generic;
using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    public class ProductTest : FullAuditedEntity<int>
    {
        #region Constructor
        public ProductTest()
        {
            TestParameters=new HashSet<TestParameter>();
        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? TestId { get; set; }
        public virtual Test Test { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<TestParameter> TestParameters { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}