﻿using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    public class Scrap : FullAuditedEntity<int>
    {
        #region Constructor
        public Scrap()
        {

        }
        #endregion Constructor

        #region Properties
        public int Amount { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations

        public int? ProductScrapId { get; set; }
        public virtual ProductScrap ProductScrap { get; set; }
        public int? ProductControlId { get; set; }
        public virtual ProductControl ProductControl { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}