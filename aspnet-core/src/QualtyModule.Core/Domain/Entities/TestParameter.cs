﻿using System.Collections.Generic;
using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    public class TestParameter:FullAuditedEntity<int>
    {
        #region Constructor
        public TestParameter()
        {
            TestResults=new HashSet<TestResult>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int? ProductTestId { get; set; }
        public virtual ProductTest ProductTest{ get; set; }
        public int? UnitId { get; set; }
        public virtual Unit Unit { get; set; }
        public int? RuleId { get; set; }
        public virtual Rule Rule { get; set; }
        public int? ValueTypeId { get; set; }
        public virtual ValueType ValueType { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<TestResult> TestResults { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}