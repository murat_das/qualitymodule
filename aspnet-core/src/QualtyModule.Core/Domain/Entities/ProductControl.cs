﻿using System.Collections.Generic;
using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    public class ProductControl:FullAuditedEntity<int>
    {
        #region Constructor
        public ProductControl()
        {
            Scraps=new HashSet<Scrap>();
            ProductControlTests=new HashSet<ProductControlTest>();
            TestResults = new HashSet<TestResult>();

        }
        #endregion Constructor

        #region Properties
        public int WorkOrderId { get; set; }
        public int Amount { get; set; }
        public bool Seeded { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        #endregion One To One Relations

        #region One To Many Relations
        public virtual ICollection<Scrap> Scraps { get; set; }
        public virtual ICollection<TestResult> TestResults { get; set; }
        public virtual ICollection<ProductControlTest> ProductControlTests { get; set; }

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}