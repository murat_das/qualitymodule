﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace QualtyModule.Domain.Entities
{
    //Tenant için IMustHaveTenant eklememiz gerekmekte. Kendi içerisinde globalfilter etkin olarak geliyor.
    //public class Product : FullAuditedEntity<int>, IMustHaveTenant
    public class Product : FullAuditedEntity<int>
    {
        #region Constructor
        public Product()
        {
            ProductControls = new HashSet<ProductControl>();
            ProductScraps = new HashSet<ProductScrap>();
            ProductTests=new HashSet<ProductTest>();

        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public virtual ICollection<ProductControl> ProductControls { get; set; }
        public virtual ICollection<ProductScrap> ProductScraps { get; set; }
        public virtual ICollection<ProductTest> ProductTests { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}