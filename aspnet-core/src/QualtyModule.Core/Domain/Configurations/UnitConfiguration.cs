﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class UnitConfiguration : IEntityTypeConfiguration<Unit>
    {
        public void Configure(EntityTypeBuilder<Unit> builder)
        {
            #region Properties

            builder.ToTable("Unit");

            builder.HasKey(unit => unit.Id);

            builder.Property(unit => unit.Name)
                .HasColumnName("Name")
                .HasMaxLength(50);

            #endregion Properties

            #region Relations
            builder.HasMany<TestParameter>(unit => unit.TestParameters)
                .WithOne(testParameter => testParameter.Unit)
                .HasForeignKey(testParameter => testParameter.UnitId)
                .OnDelete(DeleteBehavior.ClientSetNull);


            #endregion Relations

            #region OptimisticLockField
            builder.Property(unit => unit.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}