﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class RuleConfiguration : IEntityTypeConfiguration<Rule>
    {
        public void Configure(EntityTypeBuilder<Rule> builder)
        {
            #region Properties

            builder.ToTable("Rule");

            builder.HasKey(rule => rule.Id);

            builder.Property(rule => rule.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(rule => rule.Min)
                .HasColumnName("Min");
            builder.Property(rule => rule.Max)
                .HasColumnName("Max");

            #endregion Properties

            #region Relations
            builder.HasMany<TestParameter>(rule => rule.TestParameters)
                .WithOne(testParameter => testParameter.Rule)
                .HasForeignKey(testParameter => testParameter.RuleId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(rule => rule.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}