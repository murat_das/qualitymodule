﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ProductTestConfiguration : IEntityTypeConfiguration<ProductTest>
    {
        public void Configure(EntityTypeBuilder<ProductTest> builder)
        {
            #region Properties

            builder.ToTable("ProductTest");

            builder.HasKey(productTest => productTest.Id);

            #endregion Properties

            #region Relations
            builder.HasMany<TestParameter>(productTest => productTest.TestParameters)
                .WithOne(testParameter => testParameter.ProductTest)
                .HasForeignKey(testParameter => testParameter.ProductTestId)
                .OnDelete(DeleteBehavior.ClientSetNull);

          
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}