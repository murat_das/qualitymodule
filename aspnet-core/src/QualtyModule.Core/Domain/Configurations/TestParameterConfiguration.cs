﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class TestParameterConfiguration : IEntityTypeConfiguration<TestParameter>
    {
        public void Configure(EntityTypeBuilder<TestParameter> builder)
        {
            #region Properties

            builder.ToTable("TestParameter");

            builder.HasKey(testParameter => testParameter.Id);

            builder.Property(testParameter => testParameter.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations

            builder.HasMany<TestResult>(testParameter => testParameter.TestResults)
                .WithOne(testResult => testResult.TestParameter)
                .HasForeignKey(testResult => testResult.TestParameterId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(test => test.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}