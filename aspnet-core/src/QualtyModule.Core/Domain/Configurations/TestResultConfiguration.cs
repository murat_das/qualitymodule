﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class TestResultConfiguration : IEntityTypeConfiguration<TestResult>
    {
        public void Configure(EntityTypeBuilder<TestResult> builder)
        {
            #region Properties

            builder.ToTable("TestResult");

            builder.HasKey(testResult => testResult.Id);

            builder.Property(testResult => testResult.Value)
                .HasColumnName("Value")
                .HasMaxLength(150);
            builder.Property(testResult => testResult.IsOK)
                .HasColumnName("IsOK");
            builder.Property(testResult => testResult.IsOKUpdatedDate)
                .HasColumnName("IsOKUpdatedDate");

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(test => test.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}