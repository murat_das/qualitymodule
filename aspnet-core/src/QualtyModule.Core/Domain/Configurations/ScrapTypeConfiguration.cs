﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ScrapTypeConfiguration : IEntityTypeConfiguration<ScrapType>
    {
        public void Configure(EntityTypeBuilder<ScrapType> builder)
        {
            #region Properties

            builder.ToTable("ScrapType");

            builder.HasKey(scrapType => scrapType.Id);

            builder.Property(scrapType => scrapType.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            
            #endregion Properties

            #region Relations
            builder.HasMany<ProductScrap>(scrapType => scrapType.ProductScraps)
                .WithOne(productScrap => productScrap.ScrapType)
                .HasForeignKey(productScrap => productScrap.ScrapTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(scrapType => scrapType.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}