﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            #region Properties

            builder.ToTable("Product");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);


            #endregion Properties

            #region Relations
            builder.HasMany<ProductControl>(product => product.ProductControls)
                .WithOne(productControl => productControl.Product)
                .HasForeignKey(productControl => productControl.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<ProductScrap>(product => product.ProductScraps)
                .WithOne(productScrap => productScrap.Product)
                .HasForeignKey(productScrap => productScrap.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<ProductTest>(product => product.ProductTests)
                .WithOne(productTest => productTest.Product)
                .HasForeignKey(productTest => productTest.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}