﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ScrapConfiguration : IEntityTypeConfiguration<Scrap>
    {
        public void Configure(EntityTypeBuilder<Scrap> builder)
        {
            #region Properties

            builder.ToTable("Scrap");

            builder.HasKey(scrap => scrap.Id);

            builder.Property(scrap => scrap.Amount)
                .HasColumnName("Amount");


            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(scrap => scrap.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}