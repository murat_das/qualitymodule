﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ValueTypeConfiguration : IEntityTypeConfiguration<ValueType>
    {
        public void Configure(EntityTypeBuilder<ValueType> builder)
        {
            #region Properties

            builder.ToTable("ValueType");

            builder.HasKey(valueType => valueType.Id);

            builder.Property(valueType => valueType.Name)
                .HasColumnName("Name")
                .HasMaxLength(50);

            #endregion Properties

            #region Relations
            builder.HasMany<TestParameter>(valueType => valueType.TestParameters)
                .WithOne(testParameter => testParameter.ValueType)
                .HasForeignKey(testParameter => testParameter.ValueTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull);


            #endregion Relations

            #region OptimisticLockField
            builder.Property(valueType => valueType.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}