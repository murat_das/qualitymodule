﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            #region Properties

            builder.ToTable("Test");

            builder.HasKey(test => test.Id);

            builder.Property(test => test.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<ProductTest>(test => test.ProductTests)
                .WithOne(productTest => productTest.Test)
                .HasForeignKey(productTest => productTest.TestId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            builder.HasMany<ProductControlTest>(test => test.ProductControlTests)
                .WithOne(pct => pct.Test)
                .HasForeignKey(pct => pct.ProductControlId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(test => test.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}