﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ProductControlTestConfiguration : IEntityTypeConfiguration<ProductControlTest>
    {
        public void Configure(EntityTypeBuilder<ProductControlTest> builder)
        {
            #region Properties

            builder.ToTable("ProductControlTest");

            builder.HasKey(pct => pct.Id);

            builder.Property(pct => pct.IsOK)
                .HasColumnName("IsOK");

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(pct => pct.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}