﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ProductControlConfiguration : IEntityTypeConfiguration<ProductControl>
    {
        public void Configure(EntityTypeBuilder<ProductControl> builder)
        {
            #region Properties

            builder.ToTable("ProductControl");

            builder.HasKey(productControl => productControl.Id);

            builder.Property(productControl => productControl.WorkOrderId)
                .HasColumnName("WorkOrderId");

            builder.Property(productControl => productControl.Amount)
                .HasColumnName("Amount");

            builder.Property(productControl => productControl.Seeded)
                .HasColumnName("Seeded");

            #endregion Properties

            #region Relations

            builder.HasMany<Scrap>(productControl =>
                    productControl.Scraps)
                .WithOne(scrap => scrap.ProductControl)
                .HasForeignKey(scrap => scrap.ProductControlId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<ProductControlTest>(productControl =>
                    productControl.ProductControlTests)
                .WithOne(productControlTest => productControlTest.ProductControl)
                .HasForeignKey(productControlTest => productControlTest.ProductControlId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<TestResult>(productControl =>
                    productControl.TestResults)
                .WithOne(testResult => testResult.ProductControl)
                .HasForeignKey(testResult => testResult.ProductControlId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(productControl => productControl.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }

    }
}