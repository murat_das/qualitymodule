﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Domain.Configurations
{
    public class ProductScrapConfiguration : IEntityTypeConfiguration<ProductScrap>
    {
        public void Configure(EntityTypeBuilder<ProductScrap> builder)
        {
            #region Properties

            builder.ToTable("ProductScrap");

            builder.HasKey(productScrap => productScrap.Id);

            #endregion Properties

            #region Relations

            builder.HasMany<Scrap>(productScrap =>
                    productScrap.Scraps)
                .WithOne(scrap => scrap.ProductScrap)
                .HasForeignKey(scrap => scrap.ProductScrapId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(productScrap => productScrap.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}