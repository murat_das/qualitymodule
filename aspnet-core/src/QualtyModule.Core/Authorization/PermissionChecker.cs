﻿using Abp.Authorization;
using QualtyModule.Authorization.Roles;
using QualtyModule.Authorization.Users;

namespace QualtyModule.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
