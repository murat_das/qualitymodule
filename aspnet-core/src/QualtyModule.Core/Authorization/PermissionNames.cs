﻿namespace QualtyModule.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";
        public const string Pages_Users_Menu = "Pages.Users.Menu";

        public const string Pages_Roles = "Pages.Roles";

        #region ProductCotrols
        public const string ProductControl = "ProductControl";
        public const string ProductControl_Create = "ProductControl.Create";
        public const string ProductControl_Get = "ProductControl.Get";
        public const string ProductControl_GetList = "ProductControl.GetList";
        public const string ProductControl_Delete = "ProductControl.Delete";
        public const string ProductControl_Update = "ProductControl.Update";
        #endregion

        #region ProductCotrolTests
        public const string ProductControlTest = "ProductControlTest";
        public const string ProductControlTest_Create = "ProductControlTest.Create";
        public const string ProductControlTest_Get = "ProductControlTest.Get";
        public const string ProductControlTest_GetList = "ProductControlTest.GetList";
        public const string ProductControlTest_Delete = "ProductControlTest.Delete";
        public const string ProductControlTest_Update = "ProductControlTest.Update";
        #endregion

        #region Products
        public const string Product = "Product";
        public const string Product_Create = "Product.Create";
        public const string Product_Get = "Product.Get";
        public const string Product_GetList = "Product.GetList";
        public const string Product_Delete = "Product.Delete";
        public const string Product_Update = "Product.Update";
        #endregion

        #region ProductScraps
        public const string ProductScrap = "ProductScrap";
        public const string ProductScrap_Create = "ProductScrap.Create";
        public const string ProductScrap_Get = " ProductScrap.Get";
        public const string ProductScrap_GetList = "ProductScrap.GetList";
        public const string ProductScrap_Delete = "ProductScrap.Delete";
        public const string ProductScrap_Update = "ProductScrap.Update";
        #endregion

        #region ProductTests
        public const string ProductTest = "ProductTest";
        public const string ProductTest_Create = "ProductTest.Create";
        public const string ProductTest_Get = "ProductTest.Get";
        public const string ProductTest_GetList = "ProductTest.GetList";
        public const string ProductTest_Delete = "ProductTest.Delete";
        public const string ProductTest_Update = "ProductTest.Update";
        #endregion

        #region Rules
        public const string Rule = "Rule";
        public const string Rule_Create = "Rule.Create";
        public const string Rule_Get = "Rule.Get";
        public const string Rule_GetList = "Rule.GetList";
        public const string Rule_Delete = "Rule.Delete";
        public const string Rule_Update = "Rule.Update";
        #endregion

        #region Scraps
        public const string Scrap = "Scrap";
        public const string Scrap_Create = "Scrap.Create";
        public const string Scrap_Get = "Scrap.Get";
        public const string Scrap_GetList = "Scrap.GetList";
        public const string Scrap_Delete = "Scrap.Delete";
        public const string Scrap_Update = "Scrap.Update";
        #endregion

        #region ScrapTypes
        public const string ScrapType = "ScrapType";
        public const string ScrapType_Create = "ScrapType.Create";
        public const string ScrapType_Get = "ScrapType.Get";
        public const string ScrapType_GetList = "ScrapType.GetList";
        public const string ScrapType_Delete = "ScrapType.Delete";
        public const string ScrapType_Update = "ScrapType.Update";
        #endregion

        #region TestParameters
        public const string TestParameter = "TestParameter";
        public const string TestParameter_Create = "TestParameter.Create";
        public const string TestParameter_Get = "TestParameter.Get";
        public const string TestParameter_GetList = "TestParameter.GetList";
        public const string TestParameter_Delete = "TestParameter.Delete";
        public const string TestParameter_Update = "TestParameter.Update";
        #endregion

        #region TestResults
        public const string TestResult = "TestResult";
        public const string TestResult_Create = "TestResult.Create";
        public const string TestResult_Get = "TestResult.Get";
        public const string TestResult_GetList = "TestResult.GetList";
        public const string TestResult_Delete = "TestResult.Delete";
        public const string TestResult_Update = "TestResult.Update";
        #endregion

        #region Tests
        public const string Test = "Test";
        public const string Test_Create = "Test.Create";
        public const string Test_Get = "Test.Get";
        public const string Test_GetList = "Test.GetList";
        public const string Test_Delete = "Test.Delete";
        public const string Test_Update = "Test.Update";
        #endregion

        #region Units
        public const string Unit = "Unit";
        public const string Unit_Create = "Unit.Create";
        public const string Unit_Get = "Unit.Get";
        public const string Unit_GetList = "Unit.GetList";
        public const string Unit_Delete = "Unit.Delete";
        public const string Unit_Update = "Unit.Update";
        #endregion

        #region ValueTypes
        public const string ValueType = "ValueType";
        public const string ValueType_Create = "ValueType.Create";
        public const string ValueType_Get = "ValueType.Get";
        public const string ValueType_GetList = "ValueType.GetList";
        public const string ValueType_Delete = "ValueType.Delete";
        public const string ValueType_Update = "ValueType.Update";
        #endregion
    }
}
