﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace QualtyModule.Authorization
{
    public class QualtyModuleAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Users_Menu, L("Permission_Users_Menu"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            // ProductControls
            context.CreatePermission(PermissionNames.ProductControl, L("Permission_ProductControl"));
            context.CreatePermission(PermissionNames.ProductControl_Create, L("Permission_ProductControl_Create"));
            context.CreatePermission(PermissionNames.ProductControl_Get, L("Permission_ProductControl_Get"));
            context.CreatePermission(PermissionNames.ProductControl_GetList, L("Permission_ProductControl_GetList"));
            context.CreatePermission(PermissionNames.ProductControl_Delete, L("Permission_ProductControl_Delete"));
            context.CreatePermission(PermissionNames.ProductControl_Update, L("Permission_ProductControl_Update"));

            // ProductControlTests
            context.CreatePermission(PermissionNames.ProductControlTest, L("Permission_ProductControlTest"));
            context.CreatePermission(PermissionNames.ProductControlTest_Create, L("Permission_ProductControlTest_Create"));
            context.CreatePermission(PermissionNames.ProductControlTest_Get, L("Permission_ProductControlTest_Get"));
            context.CreatePermission(PermissionNames.ProductControlTest_GetList, L("Permission_ProductControlTest_GetList"));
            context.CreatePermission(PermissionNames.ProductControlTest_Delete, L("Permission_ProductControlTest_Delete"));
            context.CreatePermission(PermissionNames.ProductControlTest_Update, L("Permission_ProductControlTest_Update"));

            // Products
            context.CreatePermission(PermissionNames.Product, L("Permission_Product"));
            context.CreatePermission(PermissionNames.Product_Create, L("Permission_Product_Create"));
            context.CreatePermission(PermissionNames.Product_Get, L("Permission_Product_Get"));
            context.CreatePermission(PermissionNames.Product_GetList, L("Permission_Product_GetList"));
            context.CreatePermission(PermissionNames.Product_Delete, L("Permission_Product_Delete"));
            context.CreatePermission(PermissionNames.Product_Update, L("Permission_Product_Update"));

            // ProductScraps
            context.CreatePermission(PermissionNames.ProductScrap, L("Permission_ProductScrap"));
            context.CreatePermission(PermissionNames.ProductScrap_Create, L("Permission_ProductScrap_Create"));
            context.CreatePermission(PermissionNames.ProductScrap_Get, L("Permission_ProductScrap_Get"));
            context.CreatePermission(PermissionNames.ProductScrap_GetList, L("Permission_ProductScrap_GetList"));
            context.CreatePermission(PermissionNames.ProductScrap_Delete, L("Permission_ProductScrap_Delete"));
            context.CreatePermission(PermissionNames.ProductScrap_Update, L("Permission_ProductScrap_Update"));

            // ProductTests
            context.CreatePermission(PermissionNames.ProductTest, L("Permission_ProductTest"));
            context.CreatePermission(PermissionNames.ProductTest_Create, L("Permission_ProductTest_Create"));
            context.CreatePermission(PermissionNames.ProductTest_Get, L("Permission_ProductTest_Get"));
            context.CreatePermission(PermissionNames.ProductTest_GetList, L("Permission_ProductTest_GetList"));
            context.CreatePermission(PermissionNames.ProductTest_Delete, L("Permission_ProductTest_Delete"));
            context.CreatePermission(PermissionNames.ProductTest_Update, L("Permission_ProductTest_Update"));

            // Rules
            context.CreatePermission(PermissionNames.Rule, L("Permission_Rule"));
            context.CreatePermission(PermissionNames.Rule_Create, L("Permission_Rule_Create"));
            context.CreatePermission(PermissionNames.Rule_Get, L("Permission_Rule_Get"));
            context.CreatePermission(PermissionNames.Rule_GetList, L("Permission_Rule_GetList"));
            context.CreatePermission(PermissionNames.Rule_Delete, L("Permission_Rule_Delete"));
            context.CreatePermission(PermissionNames.Rule_Update, L("Permission_Rule_Update"));

            // Scraps
            context.CreatePermission(PermissionNames.Scrap, L("Permission_Scrap"));
            context.CreatePermission(PermissionNames.Scrap_Create, L("Permission_Scrap_Create"));
            context.CreatePermission(PermissionNames.Scrap_Get, L("Permission_Scrap_Get"));
            context.CreatePermission(PermissionNames.Scrap_GetList, L("Permission_Scrap_GetList"));
            context.CreatePermission(PermissionNames.Scrap_Delete, L("Permission_Scrap_Delete"));
            context.CreatePermission(PermissionNames.Scrap_Update, L("Permission_Scrap_Update"));

            // ScrapTypes
            context.CreatePermission(PermissionNames.ScrapType, L("Permission_ScrapType"));
            context.CreatePermission(PermissionNames.ScrapType_Create, L("Permission_ScrapType_Create"));
            context.CreatePermission(PermissionNames.ScrapType_Get, L("Permission_ScrapType_Get"));
            context.CreatePermission(PermissionNames.ScrapType_GetList, L("Permission_ScrapType_GetList"));
            context.CreatePermission(PermissionNames.ScrapType_Delete, L("Permission_ScrapType_Delete"));
            context.CreatePermission(PermissionNames.ScrapType_Update, L("Permission_ScrapType_Update"));

            // TestParameters
            context.CreatePermission(PermissionNames.TestParameter, L("Permission_TestParameter"));
            context.CreatePermission(PermissionNames.TestParameter_Create, L("Permission_TestParameter_Create"));
            context.CreatePermission(PermissionNames.TestParameter_Get, L("Permission_TestParameter_Get"));
            context.CreatePermission(PermissionNames.TestParameter_GetList, L("Permission_TestParameter_GetList"));
            context.CreatePermission(PermissionNames.TestParameter_Delete, L("Permission_TestParameter_Delete"));
            context.CreatePermission(PermissionNames.TestParameter_Update, L("Permission_TestParameter_Update"));

            // TestResults
            context.CreatePermission(PermissionNames.TestResult, L("Permission_TestResult"));
            context.CreatePermission(PermissionNames.TestResult_Create, L("Permission_TestResult_Create"));
            context.CreatePermission(PermissionNames.TestResult_Get, L("Permission_TestResult_Get"));
            context.CreatePermission(PermissionNames.TestResult_GetList, L("Permission_TestResult_GetList"));
            context.CreatePermission(PermissionNames.TestResult_Delete, L("Permission_TestResult_Delete"));
            context.CreatePermission(PermissionNames.TestResult_Update, L("Permission_TestResult_Update"));

            // Tests
            context.CreatePermission(PermissionNames.Test, L("Permission_Test"));
            context.CreatePermission(PermissionNames.Test_Create, L("Permission_Test_Create"));
            context.CreatePermission(PermissionNames.Test_Get, L("Permission_Test_Get"));
            context.CreatePermission(PermissionNames.Test_GetList, L("Permission_Test_GetList"));
            context.CreatePermission(PermissionNames.Test_Delete, L("Permission_Test_Delete"));
            context.CreatePermission(PermissionNames.Test_Update, L("Permission_Test_Update"));

            // Units
            context.CreatePermission(PermissionNames.Unit, L("Permission_Unit"));
            context.CreatePermission(PermissionNames.Unit_Create, L("Permission_Unit_Create"));
            context.CreatePermission(PermissionNames.Unit_Get, L("Permission_Unit_Get"));
            context.CreatePermission(PermissionNames.Unit_GetList, L("Permission_Unit_GetList"));
            context.CreatePermission(PermissionNames.Unit_Delete, L("Permission_Unit_Delete"));
            context.CreatePermission(PermissionNames.Unit_Update, L("Permission_Unit_Update"));

            // ValueTypes
            context.CreatePermission(PermissionNames.ValueType, L("Permission_ValueType"));
            context.CreatePermission(PermissionNames.ValueType_Create, L("Permission_ValueType_Create"));
            context.CreatePermission(PermissionNames.ValueType_Get, L("Permission_ValueType_Get"));
            context.CreatePermission(PermissionNames.ValueType_GetList, L("Permission_ValueType_GetList"));
            context.CreatePermission(PermissionNames.ValueType_Delete, L("Permission_ValueType_Delete"));
            context.CreatePermission(PermissionNames.ValueType_Update, L("Permission_ValueType_Update"));

        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, QualtyModuleConsts.LocalizationSourceName);
        }
    }
}
