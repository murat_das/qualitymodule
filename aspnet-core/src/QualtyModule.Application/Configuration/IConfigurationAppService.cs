﻿using System.Threading.Tasks;
using QualtyModule.Configuration.Dto;

namespace QualtyModule.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
