﻿using System;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.Domain.TestParameters.Dtos;

namespace QualtyModule.Domain.TestResults.Dtos
{
    public class CreateTestResultInput
    {
        public string Value { get; set; }
        public bool IsOK { get; set; }
        public int? IsOKUpdatedUserId { get; set; }
        public DateTime IsOKUpdatedDate { get; set; }
        public TestParameterPartOutPut TestParameter { get; set; }
        public ProductControlPartOutPut ProductControl { get; set; }
    }
}