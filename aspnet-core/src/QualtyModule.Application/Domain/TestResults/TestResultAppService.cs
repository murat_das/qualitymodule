﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.TestResults.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.TestResults
{
    [AbpAuthorize(PermissionNames.TestResult)]
    public class TestResultAppService:QualtyModuleAppServiceBase,ITestResultAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITestResultRepository _testResultRepository;

        public TestResultAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITestResultRepository testResultRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _testResultRepository = testResultRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_Create)]
        public async Task<TestResultFullOutPut> CreateAsync(CreateTestResultInput input)
        {
            var testResult = new TestResult()
            {
               Value = input.Value,
               IsOK = input.IsOK,
               IsOKUpdatedUserId=input.IsOKUpdatedUserId,
               IsOKUpdatedDate=DateTime.Now,
               TestParameterId = input.TestParameter?.Id,
               ProductControlId = input.ProductControl?.Id
            };

            await _testResultRepository.InsertAsync(testResult);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestResultFullOutPut>(testResult);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_Get)]
        public async Task<TestResultFullOutPut> GetAsync(GetTestResultInput input)
        {
            var testResult = await _entityManager.GetTestResultAsync(input.Id);

            return ObjectMapper.Map<TestResultFullOutPut>(testResult);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_GetList)]
        public async Task<List<TestResultFullOutPut>> GetListAsync()
        {
            var testResultList = await _testResultRepository.GetAllListAsync();

            return ObjectMapper.Map<List<TestResultFullOutPut>>(testResultList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_Delete)]
        public async Task DeleteAsync(DeleteTestResultInput input)
        {
            var testResult = await _entityManager.GetTestResultAsync(input.Id);

            await _testResultRepository.DeleteAsync(testResult);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_Update)]
        public async Task<TestResultFullOutPut> UpdateAsync(UpdateTestResultInput input)
        {
            var testResult = await _entityManager.GetTestResultAsync(input.Id);

            testResult.Value = input.Value;
            testResult.IsOK = input.IsOK;
            testResult.IsOKUpdatedUserId = input.IsOKUpdatedUserId;
            testResult.IsOKUpdatedDate = DateTime.Now;
            testResult.TestParameterId = input.TestParameter?.Id;
            testResult.ProductControlId = input.ProductControl?.Id;

            await _testResultRepository.UpdateAsync(testResult);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestResultFullOutPut>(testResult);
        }
    }
}