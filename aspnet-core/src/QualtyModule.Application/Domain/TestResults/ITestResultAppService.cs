﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.TestResults.Dtos;

namespace QualtyModule.Domain.TestResults
{
    public interface ITestResultAppService : IApplicationService
    {
        #region Async Methods
        Task<TestResultFullOutPut> CreateAsync(CreateTestResultInput input);
        Task<TestResultFullOutPut> GetAsync(GetTestResultInput input);
        Task<List<TestResultFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteTestResultInput input);
        Task<TestResultFullOutPut> UpdateAsync(UpdateTestResultInput input);
        #endregion Async Methods
    }
}