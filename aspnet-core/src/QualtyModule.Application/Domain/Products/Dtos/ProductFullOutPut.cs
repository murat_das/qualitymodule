﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.Domain.ProductScraps.Dtos;
using QualtyModule.Domain.ProductTests.Dtos;

namespace QualtyModule.Domain.Products.Dtos
{
    public class ProductFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public List<ProductControlPartOutPut> ProductControls { get; set; }
        public List<ProductScrapPartOutPut> ProductScraps { get; set; }
        public List<ProductTestPartOutPut> ProductTests { get; set; }

    }
}