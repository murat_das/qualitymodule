﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.Products.Dtos
{
    public class ProductPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}