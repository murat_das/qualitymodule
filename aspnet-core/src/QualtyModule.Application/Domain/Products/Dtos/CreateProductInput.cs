﻿namespace QualtyModule.Domain.Products.Dtos
{
    public class CreateProductInput
    {
        public string Name { get; set; }
    }
}