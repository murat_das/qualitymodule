﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.Products.Dtos
{
    public class UpdateProductInput : EntityDto<int>
    {
        public string Name { get; set; }
    }
}