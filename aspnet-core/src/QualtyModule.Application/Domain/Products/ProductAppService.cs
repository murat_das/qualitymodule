﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.Products
{
    [AbpAuthorize(PermissionNames.Product)]
    public class ProductAppService : QualtyModuleAppServiceBase, IProductAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductRepository _productRepository;

        public ProductAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductRepository productRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productRepository = productRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Product_Create)]
        public async Task<ProductFullOutPut> CreateAsync(CreateProductInput input)
        {
            var product = new Product()
            {
                Name = input.Name
            };

            await _productRepository.InsertAsync(product);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductFullOutPut>(product);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Product_Get)]
        public async Task<ProductFullOutPut> GetAsync(GetProductInput input)
        {
            var product = await _entityManager.GetProductAsync(input.Id);

            return ObjectMapper.Map<ProductFullOutPut>(product);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Product_GetList)]
        public async Task<List<ProductFullOutPut>> GetListAsync()
        {
           
            var productList = await _productRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductFullOutPut>>(productList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Product_Delete)]
        public async Task DeleteAsync(DeleteProductInput input)
        {
            var product = await _entityManager.GetProductAsync(input.Id);

            await _productRepository.DeleteAsync(product.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Product_Update)]
        public async Task<ProductFullOutPut> UpdateAsync(UpdateProductInput input)
        {
            var product = await _entityManager.GetProductAsync(input.Id);

            product.Name = input.Name.IsNullOrEmpty() ? product.Name : input.Name;

            await _productRepository.UpdateAsync(product);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductFullOutPut>(product);
        }
    }
}