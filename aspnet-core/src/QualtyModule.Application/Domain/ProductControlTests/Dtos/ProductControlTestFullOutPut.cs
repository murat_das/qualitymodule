﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.Domain.Tests.Dtos;

namespace QualtyModule.Domain.ProductControlTests.Dtos
{
    public class ProductControlTestFullOutPut:EntityDto<int>
    {
        public bool IsOK { get; set; }
        public ProductControlPartOutPut ProductControl { get; set; }
        public TestPartOutPut Test { get; set; }
    }
}