﻿namespace QualtyModule.Domain.ProductControlTests.Dtos
{
    public class ProductControlTestPartOutPut
    {
        public bool IsOK { get; set; }
    }
}