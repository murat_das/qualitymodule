﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.ProductControlTests.Dtos;

namespace QualtyModule.Domain.ProductControlTests
{
    public interface IProductControlTestAppService : IApplicationService
    {
        #region Async Methods
        Task<ProductControlTestFullOutPut> CreateAsync(CreateProductControlTestInput input);
        Task<ProductControlTestFullOutPut> GetAsync(GetProductControlTestInput input);
        Task<List<ProductControlTestFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductControlTestInput input);
        Task<ProductControlTestFullOutPut> UpdateAsync(UpdateProductControlTestInput input);
        #endregion Async Methods
    }
}