﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ProductControlTests.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.ProductControlTests
{
    [AbpAuthorize(PermissionNames.ProductControlTest)]
    public class ProductControlTestAppService:QualtyModuleAppServiceBase,IProductControlTestAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductControlTestRepository _productControlTestRepository;

        public ProductControlTestAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductControlTestRepository productControlTestRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productControlTestRepository = productControlTestRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControlTest_Create)]
        public async Task<ProductControlTestFullOutPut> CreateAsync(CreateProductControlTestInput input)
        {
            var productControlTest = new ProductControlTest()
            {
                ProductControlId = input.ProductControl?.Id,
                TestId = input.Test?.Id,
                IsOK = input.IsOK
            };

            await _productControlTestRepository.InsertAsync(productControlTest);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductControlTestFullOutPut>(productControlTest);
        }
        [AbpAuthorize(PermissionNames.ProductControlTest_Get)]
        public async Task<ProductControlTestFullOutPut> GetAsync(GetProductControlTestInput input)
        {
            var productControlTest =await _entityManager.GetProductControlTestAsync(input.Id);

            return ObjectMapper.Map<ProductControlTestFullOutPut>(productControlTest);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControlTest_GetList)]
        public async Task<List<ProductControlTestFullOutPut>> GetListAsync()
        {
            var productControlTestList = await _productControlTestRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductControlTestFullOutPut>>(productControlTestList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControlTest_Delete)]
        public async Task DeleteAsync(DeleteProductControlTestInput input)
        {
            var productCotrolTest = await _entityManager.GetProductControlTestAsync(input.Id);

            await _productControlTestRepository.DeleteAsync(productCotrolTest);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControlTest_Update)]
        public async Task<ProductControlTestFullOutPut> UpdateAsync(UpdateProductControlTestInput input)
        {
            var productControlTest =await _entityManager.GetProductControlTestAsync(input.Id);

            productControlTest.ProductControlId = input.ProductControl?.Id;
            productControlTest.TestId = input.Test?.Id;
            productControlTest.IsOK = input.IsOK;

            await _productControlTestRepository.UpdateAsync(productControlTest);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductControlTestFullOutPut>(productControlTest);
        }
    }
}