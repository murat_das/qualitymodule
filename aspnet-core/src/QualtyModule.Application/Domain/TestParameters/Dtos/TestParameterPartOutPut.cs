﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.TestParameters.Dtos
{
    public class TestParameterPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}