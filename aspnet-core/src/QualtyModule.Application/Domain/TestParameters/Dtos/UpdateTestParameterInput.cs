﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductTests.Dtos;
using QualtyModule.Domain.Rules.Dtos;
using QualtyModule.Domain.Units.Dtos;
using QualtyModule.Domain.ValueTypes.Dtos;

namespace QualtyModule.Domain.TestParameters.Dtos
{
    public class UpdateTestParameterInput:EntityDto<int>
    {
        public string Name { get; set; }
        public ProductTestPartOutPut ProductTest { get; set; }
        public UnitPartOutPut Unit { get; set; }
        public RulePartOutPut Rule { get; set; }
        public ValueTypePartOutPut ValueType { get; set; }
    }
}