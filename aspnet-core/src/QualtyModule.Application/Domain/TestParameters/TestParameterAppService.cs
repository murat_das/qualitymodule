﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.TestParameters.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.TestParameters
{
    [AbpAuthorize(PermissionNames.TestParameter)]
    public class TestParameterAppService : QualtyModuleAppServiceBase, ITestParameterAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITestParameterRepository _testParameterRepository;

        public TestParameterAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITestParameterRepository testParameterRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _testParameterRepository = testParameterRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.TestParameter_Create)]
        public async Task<TestParameterFullOutPut> CreateAsync(CreateTestParameterInput input)
        {
            var testParameter = new TestParameter()
            {
                Name = input.Name,
                ProductTestId = input.ProductTest?.Id,
                RuleId = input.Rule?.Id,
                UnitId = input.Unit?.Id,
                ValueTypeId = input.ValueType?.Id
            };

            await _testParameterRepository.InsertAsync(testParameter);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestParameterFullOutPut>(testParameter);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestParameter_Get)]
        public async Task<TestParameterFullOutPut> GetAsync(GetTestParameterInput input)
        {
            var testParameter = await _entityManager.GetTestParameterAsync(input.Id);

            return ObjectMapper.Map<TestParameterFullOutPut>(testParameter);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestParameter_GetList)]
        public async Task<List<TestParameterFullOutPut>> GetListAsync()
        {
            var testParameterList = await _testParameterRepository.GetAllListAsync();

            return ObjectMapper.Map<List<TestParameterFullOutPut>>(testParameterList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestParameter_Delete)]
        public async Task DeleteAsync(DeleteTestParameterInput input)
        {
            var testParameter = await _entityManager.GetTestParameterAsync(input.Id);

            await _testParameterRepository.DeleteAsync(testParameter);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestParameter_Update)]
        public async Task<TestParameterFullOutPut> UpdateAsync(UpdateTestParameterInput input)
        {
            var testParameter = await _entityManager.GetTestParameterAsync(input.Id);

            testParameter.Name = input.Name.IsNullOrEmpty() ? testParameter.Name : input.Name;
            testParameter.ProductTestId = input.ProductTest?.Id;
            testParameter.UnitId = input.Unit?.Id;
            testParameter.ValueTypeId = input.ValueType?.Id;
            testParameter.RuleId = input.Rule?.Id;

            await _testParameterRepository.UpdateAsync(testParameter);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestParameterFullOutPut>(testParameter);
        }
    }
}