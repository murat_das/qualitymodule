﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.TestParameters.Dtos;

namespace QualtyModule.Domain.TestParameters
{
    public interface ITestParameterAppService : IApplicationService
    {
        #region Async Methods
        Task<TestParameterFullOutPut> CreateAsync(CreateTestParameterInput input);
        Task<TestParameterFullOutPut> GetAsync(GetTestParameterInput input);
        Task<List<TestParameterFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteTestParameterInput input);
        Task<TestParameterFullOutPut> UpdateAsync(UpdateTestParameterInput input);
        #endregion Async Methods
    }
}