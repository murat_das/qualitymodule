﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ProductTests.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.ProductTests
{
    [AbpAuthorize(PermissionNames.ProductTest)]
    public class ProductTestAppService:QualtyModuleAppServiceBase,IProductTestAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductTestRepository _productTestRepository;

        public ProductTestAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductTestRepository productTestRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productTestRepository = productTestRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductTest_Create)]
        public async Task<ProductTestFullOutPut> CreateAsync(CreateProductTestInput input)
        {
            var productTest=new ProductTest()
            {
                ProductId = input.Product?.Id,
                TestId = input.Test?.Id
            };

            await _productTestRepository.InsertAsync(productTest);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductTestFullOutPut>(productTest);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductTest_Get)]
        public async Task<ProductTestFullOutPut> GetAsync(GetProductTestInput input)
        {
            var productTest = await _entityManager.GetProductTestAsync(input.Id);

            return ObjectMapper.Map<ProductTestFullOutPut>(productTest);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductTest_GetList)]
        public async Task<List<ProductTestFullOutPut>> GetListAsync()
        {
            var productTestList = await _productTestRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductTestFullOutPut>>(productTestList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductTest_Delete)]
        public async Task DeleteAsync(DeleteProductTestInput input)
        {
            var productTest =await _entityManager.GetProductTestAsync(input.Id);

            await _productTestRepository.DeleteAsync(productTest);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductTest_Update)]
        public async Task<ProductTestFullOutPut> UpdateAsync(UpdateProductTestInput input)
        {
            var productTest = await _entityManager.GetProductTestAsync(input.Id);

            productTest.ProductId = input.Product?.Id;
            productTest.TestId = input.Test?.Id;

            await _productTestRepository.UpdateAsync(productTest);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductTestFullOutPut>(productTest);
        }
    }
}