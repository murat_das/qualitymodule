﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.ProductTests.Dtos;

namespace QualtyModule.Domain.ProductTests
{
    public interface IProductTestAppService : IApplicationService
    {
        #region Async Methods
        Task<ProductTestFullOutPut> CreateAsync(CreateProductTestInput input);
        Task<ProductTestFullOutPut> GetAsync(GetProductTestInput input);
        Task<List<ProductTestFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductTestInput input);
        Task<ProductTestFullOutPut> UpdateAsync(UpdateProductTestInput input);
        #endregion Async Methods
    }
}