﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.TestParameters.Dtos;
using QualtyModule.Domain.Tests.Dtos;

namespace QualtyModule.Domain.ProductTests.Dtos
{
    public class ProductTestFullOutPut:EntityDto<int>
    {
        public ProductPartOutPut Product { get; set; }
        public TestPartOutPut Test { get; set; }
        public List<TestParameterPartOutPut> TestParameter { get; set; }
    }
}