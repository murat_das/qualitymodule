﻿using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.Tests.Dtos;

namespace QualtyModule.Domain.ProductTests.Dtos
{
    public class CreateProductTestInput
    {
        public ProductPartOutPut Product { get; set; }
        public TestPartOutPut Test { get; set; }
    }
}