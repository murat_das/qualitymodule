﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.Scraps.Dtos;

namespace QualtyModule.Domain.Scraps
{
    public interface IScrapAppService : IApplicationService
    {
        #region Async Methods
        Task<ScrapFullOutPut> CreateAsync(CreateScrapInput input);
        Task<ScrapFullOutPut> GetAsync(GetScrapInput input);
        Task<List<ScrapFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteScrapInput input);
        Task<ScrapFullOutPut> UpdateAsync(UpdateScrapInput input);
        #endregion Async Methods
    }
}