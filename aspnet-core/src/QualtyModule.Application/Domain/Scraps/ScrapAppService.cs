﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.Rules.Dtos;
using QualtyModule.Domain.Scraps.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.Scraps
{
    [AbpAuthorize(PermissionNames.Scrap)]
    public class ScrapAppService:QualtyModuleAppServiceBase,IScrapAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IScrapRepository _scrapRepository;

        public ScrapAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IScrapRepository scrapRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _scrapRepository = scrapRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Scrap_Create)]
        public async Task<ScrapFullOutPut> CreateAsync(CreateScrapInput input)
        {
            var scrap = new Scrap()
            {
                Amount = input.Amount,
                ProductControlId = input.ProductControl?.Id,
                ProductScrapId = input.ProductScrap?.Id
            };
            try
            {
                await _scrapRepository.InsertAsync(scrap);
                await _unitOfWorkManager.Current.SaveChangesAsync();

            }
            catch (Exception e)
            {
                var sadsa = e;
            }
           

            return ObjectMapper.Map<ScrapFullOutPut>(scrap);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Scrap_Get)]
        public async Task<ScrapFullOutPut> GetAsync(GetScrapInput input)
        {
            var scrap = await _entityManager.GetScrapAsync(input.Id);

            return ObjectMapper.Map<ScrapFullOutPut>(scrap);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Scrap_GetList)]
        public async Task<List<ScrapFullOutPut>> GetListAsync()
        {
            var scrapList = await _scrapRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ScrapFullOutPut>>(scrapList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Scrap_Delete)]
        public async Task DeleteAsync(DeleteScrapInput input)
        {
            var scrap =await _entityManager.GetScrapAsync(input.Id);

            await _scrapRepository.DeleteAsync(scrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Scrap_Update)]
        public async Task<ScrapFullOutPut> UpdateAsync(UpdateScrapInput input)
        {
            var scrap = await _entityManager.GetScrapAsync(input.Id);

            scrap.Amount = input.Amount;
            scrap.ProductControlId = input.ProductControl?.Id;
            scrap.ProductScrapId = input.ProductScrap?.Id;

            await _scrapRepository.UpdateAsync(scrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapFullOutPut>(scrap);
        }
    }
}