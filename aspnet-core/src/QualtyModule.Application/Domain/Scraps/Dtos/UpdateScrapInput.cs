﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.Domain.ProductScraps.Dtos;

namespace QualtyModule.Domain.Scraps.Dtos
{
    public class UpdateScrapInput : EntityDto<int>
    {
        public int Amount { get; set; }
        public ProductScrapPartOutPut ProductScrap { get; set; }
        public ProductControlPartOutPut ProductControl { get; set; }
    }
}