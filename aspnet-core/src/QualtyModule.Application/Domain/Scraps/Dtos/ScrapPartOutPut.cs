﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.Scraps.Dtos
{
    public class ScrapPartOutPut:EntityDto<int>
    {
        public int Amount { get; set; }
    }
}