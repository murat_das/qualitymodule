﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.Rules.Dtos;

namespace QualtyModule.Domain.Rules
{
    public interface IRuleAppService : IApplicationService
    {
        #region Async Methods
        Task<RuleFullOutPut> CreateAsync(CreateRuleInput input);
        Task<RuleFullOutPut> GetAsync(GetRuleInput input);
        Task<List<RuleFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteRuleInput input);
        Task<RuleFullOutPut> UpdateAsync(UpdateRuleInput input);
        #endregion Async Methods
    }
}