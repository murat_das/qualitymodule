﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Extensions;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.Rules.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.Rules
{
    [AbpAuthorize(PermissionNames.Rule)]
    public class RuleAppService:QualtyModuleAppServiceBase,IRuleAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IRuleRepository _ruleRepository;

        public RuleAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IRuleRepository ruleRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _ruleRepository = ruleRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Rule_Create)]
        public async Task<RuleFullOutPut> CreateAsync(CreateRuleInput input)
        {
            var rule = new Rule()
            {
                Name = input.Name,
                Min = input.Min,
                Max = input.Max
                
            };

            await _ruleRepository.InsertAsync(rule);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<RuleFullOutPut>(rule);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Rule_Get)]
        public async Task<RuleFullOutPut> GetAsync(GetRuleInput input)
        {
            var rule = await _entityManager.GetRuleAsync(input.Id);

            return ObjectMapper.Map<RuleFullOutPut>(rule);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Rule_GetList)]
        public async Task<List<RuleFullOutPut>> GetListAsync()
        {
            var ruleList = await _ruleRepository.GetAllListAsync();

            return ObjectMapper.Map<List<RuleFullOutPut>>(ruleList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Rule_Delete)]
        public async Task DeleteAsync(DeleteRuleInput input)
        {
            var rule =await _entityManager.GetRuleAsync(input.Id);

            await _ruleRepository.DeleteAsync(rule);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Rule_Update)]
        public async Task<RuleFullOutPut> UpdateAsync(UpdateRuleInput input)
        {
            var rule = await _entityManager.GetRuleAsync(input.Id);

            rule.Name = input.Name.IsNullOrEmpty() ? rule.Name : input.Name;
            rule.Min = input.Min;
            rule.Max = input.Max;

            await _ruleRepository.UpdateAsync(rule);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<RuleFullOutPut>(rule);
        }
    }
}