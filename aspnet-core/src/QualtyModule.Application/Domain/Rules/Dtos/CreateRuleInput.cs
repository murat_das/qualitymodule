﻿namespace QualtyModule.Domain.Rules.Dtos
{
    public class CreateRuleInput
    {
        public string Name { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
    }
}