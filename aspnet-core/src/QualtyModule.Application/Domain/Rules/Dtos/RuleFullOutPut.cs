﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.TestParameters.Dtos;

namespace QualtyModule.Domain.Rules.Dtos
{
    public class RuleFullOutPut: EntityDto<int>
    {
        public string Name { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public List<TestParameterPartOutPut> TestParameters { get; set; }
    }
}