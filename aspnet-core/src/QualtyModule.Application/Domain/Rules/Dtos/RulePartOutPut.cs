﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.Rules.Dtos
{
    public class RulePartOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
    }
}