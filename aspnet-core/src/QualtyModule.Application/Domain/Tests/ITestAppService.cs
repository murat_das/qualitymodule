﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.Tests.Dtos;

namespace QualtyModule.Domain.Tests
{
    public interface ITestAppService : IApplicationService
    {
        #region Async Methods
        Task<TestFullOutPut> CreateAsync(CreateTestInput input);
        Task<TestFullOutPut> GetAsync(GetTestInput input);
        Task<List<TestFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteTestInput input);
        Task<TestFullOutPut> UpdateAsync(UpdateTestInput input);
        #endregion Async Methods
    }
}