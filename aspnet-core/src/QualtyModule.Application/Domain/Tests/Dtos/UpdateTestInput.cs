﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.Tests.Dtos
{
    public class UpdateTestInput:EntityDto<int>
    {
        public string Name { get; set; }
    }
}