﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductControlTests.Dtos;
using QualtyModule.Domain.ProductTests.Dtos;

namespace QualtyModule.Domain.Tests.Dtos
{
    public class TestFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public List<ProductTestPartOutPut> ProductTests { get; set; }
        public List<ProductControlTestPartOutPut> ProductControlTests { get; set; }
    }
}