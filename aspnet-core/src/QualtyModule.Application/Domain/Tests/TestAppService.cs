﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Extensions;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.Tests.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.Tests
{
    [AbpAuthorize(PermissionNames.Test)]
    public class TestAppService : QualtyModuleAppServiceBase, ITestAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITestRepository _testRepository;

        public TestAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITestRepository testRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _testRepository = testRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Test_Create)]
        public async Task<TestFullOutPut> CreateAsync(CreateTestInput input)
        {
            var test = new Test()
            {
                Name = input.Name,
            };

            await _testRepository.InsertAsync(test);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestFullOutPut>(test);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Test_Get)]
        public async Task<TestFullOutPut> GetAsync(GetTestInput input)
        {
            var test = await _entityManager.GetTestAsync(input.Id);

            return ObjectMapper.Map<TestFullOutPut>(test);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Test_GetList)]
        public async Task<List<TestFullOutPut>> GetListAsync()
        {
            var testList = await _testRepository.GetAllListAsync();

            return ObjectMapper.Map<List<TestFullOutPut>>(testList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Test_Delete)]
        public async Task DeleteAsync(DeleteTestInput input)
        {
            var test = await _entityManager.GetTestAsync(input.Id);

            await _testRepository.DeleteAsync(test);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Test_Update)]
        public async Task<TestFullOutPut> UpdateAsync(UpdateTestInput input)
        {
            var test = await _entityManager.GetTestAsync(input.Id);

            test.Name = input.Name.IsNullOrEmpty() ? test.Name : input.Name;

            await _testRepository.UpdateAsync(test);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestFullOutPut>(test);
        }
    }
}