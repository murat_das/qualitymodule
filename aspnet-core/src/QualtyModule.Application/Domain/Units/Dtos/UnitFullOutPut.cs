﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.TestParameters.Dtos;

namespace QualtyModule.Domain.Units.Dtos
{
    public class UnitFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public List<TestParameterPartOutPut> TestParameters { get; set; }

    }
}