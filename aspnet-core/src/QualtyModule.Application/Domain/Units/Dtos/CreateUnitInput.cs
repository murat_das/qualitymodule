﻿namespace QualtyModule.Domain.Units.Dtos
{
    public class CreateUnitInput
    {
        public string Name { get; set; }
    }
}