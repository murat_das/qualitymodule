﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.Units.Dtos
{
    public class UpdateUnitInput:EntityDto<int>
    {
        public string Name { get; set; }
    }
}