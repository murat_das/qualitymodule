﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Extensions;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.Units.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.Units
{
    [AbpAuthorize(PermissionNames.Unit)]
    public class UnitAppService : QualtyModuleAppServiceBase, IUnitAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IUnitRepository _unitRepository;

        public UnitAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IUnitRepository unitRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _unitRepository = unitRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Unit_Create)]
        public async Task<UnitFullOutPut> CreateAsync(CreateUnitInput input)
        {
            var unit = new Unit()
            {
                Name = input.Name,
            };

            await _unitRepository.InsertAsync(unit);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<UnitFullOutPut>(unit);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Unit_Get)]
        public async Task<UnitFullOutPut> GetAsync(GetUnitInput input)
        {
            var unit = await _entityManager.GetUnitAsync(input.Id);

            return ObjectMapper.Map<UnitFullOutPut>(unit);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Unit_GetList)]
        public async Task<List<UnitFullOutPut>> GetListAsync()
        {
            var unitList = await _unitRepository.GetAllListAsync();

            return ObjectMapper.Map<List<UnitFullOutPut>>(unitList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Unit_Delete)]
        public async Task DeleteAsync(DeleteUnitInput input)
        {
            var unit = await _entityManager.GetUnitAsync(input.Id);

            await _unitRepository.DeleteAsync(unit);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Unit_Update)]
        public async Task<UnitFullOutPut> UpdateAsync(UpdateUnitInput input)
        {
            var unit = await _entityManager.GetUnitAsync(input.Id);

            unit.Name = input.Name.IsNullOrEmpty() ? unit.Name : input.Name;

            await _unitRepository.UpdateAsync(unit);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<UnitFullOutPut>(unit);
        }
    }
}