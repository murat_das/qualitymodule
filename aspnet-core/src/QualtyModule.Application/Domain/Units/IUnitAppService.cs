﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.Units.Dtos;

namespace QualtyModule.Domain.Units
{
    public interface IUnitAppService : IApplicationService
    {
        #region Async Methods
        Task<UnitFullOutPut> CreateAsync(CreateUnitInput input);
        Task<UnitFullOutPut> GetAsync(GetUnitInput input);
        Task<List<UnitFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteUnitInput input);
        Task<UnitFullOutPut> UpdateAsync(UpdateUnitInput input);
        #endregion Async Methods
    }
}