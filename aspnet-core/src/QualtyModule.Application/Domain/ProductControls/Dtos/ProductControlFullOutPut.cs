﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductControlTests.Dtos;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.Scraps.Dtos;
using QualtyModule.Domain.TestResults.Dtos;

namespace QualtyModule.Domain.ProductControls.Dtos
{
    public class ProductControlFullOutPut:EntityDto<int>
    {
        public int WorkOrderId { get; set; }
        public int Amount { get; set; }
        public bool Seeded { get; set; }

        public ProductFullOutPut Product { get; set; }

        public List<ScrapPartOutPut> Scraps { get; set; }
        public List<ProductControlTestPartOutPut> ProductControlTests { get; set; }
        public List<TestResultPartOutPut> TestResults { get; set; }
    }
}