﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.Products.Dtos;

namespace QualtyModule.Domain.ProductControls.Dtos
{
    public class UpdateProductControlInput: EntityDto<int>
    {
        public int WorkOrderId { get; set; }
        public int Amount { get; set; }
        public bool Seeded { get; set; }
        public ProductPartOutPut Product { get; set; }
    }
}