﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.Products.Dtos;

namespace QualtyModule.Domain.ProductControls.Dtos
{
    public class ProductControlPartOutPut : EntityDto<int>
    {
        public int Amount { get; set; }
        public ProductPartOutPut Product { get; set; }
    }
}