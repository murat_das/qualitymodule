﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.ProductControls
{
    [AbpAuthorize(PermissionNames.ProductControl)]
    public class ProductControlAppService:QualtyModuleAppServiceBase,IProductControlAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductControlRepository _productControlRepository;


        public ProductControlAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager, 
            IProductControlRepository productControlRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productControlRepository = productControlRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControl_Create)]
        public async Task<ProductControlFullOutPut> CreateAsync(CreateProductControlInput input)
        {
            var productControl = new ProductControl()
            {
                Amount = input.Amount,
                Seeded = input.Seeded,
                WorkOrderId = input.WorkOrderId,
                ProductId = input.Product?.Id
            };

            await _productControlRepository.InsertAsync(productControl);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductControlFullOutPut>(productControl);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControl_Get)]
        public async Task<ProductControlFullOutPut> GetAsync(GetProductControlInput input)
        {
            var productControl =await _entityManager.GetProductControlAsync(input.Id);

            return ObjectMapper.Map<ProductControlFullOutPut>(productControl);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControl_GetList)]
        public async Task<List<ProductControlFullOutPut>> GetListAsync()
        {
            var productList =await _productControlRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductControlFullOutPut>>(productList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControl_Delete)]
        public async Task DeleteAsync(DeleteProductControlInput input)
        {
            var productControl =await _entityManager.GetProductControlAsync(input.Id);

            await _productControlRepository.DeleteAsync(productControl);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductControl_Update)]
        public async Task<ProductControlFullOutPut> UpdateAsync(UpdateProductControlInput input)
        {
            var productControl =await _entityManager.GetProductControlAsync(input.Id);

            productControl.Amount = input.Amount==0?productControl.Amount:input.Amount;
            productControl.WorkOrderId = input.WorkOrderId == 0 ? productControl.WorkOrderId : input.WorkOrderId;
            productControl.ProductId = input.Product?.Id;
            productControl.Seeded = input.Seeded;

            await _productControlRepository.UpdateAsync(productControl);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductControlFullOutPut>(productControl);
        }
    }
}