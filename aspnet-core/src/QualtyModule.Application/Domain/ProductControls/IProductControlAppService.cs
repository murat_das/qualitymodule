﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.ProductControls.Dtos;

namespace QualtyModule.Domain.ProductControls
{
    public interface IProductControlAppService:IApplicationService
    {
        #region Async Methods
        Task<ProductControlFullOutPut> CreateAsync(CreateProductControlInput input);
        Task<ProductControlFullOutPut> GetAsync(GetProductControlInput input);
        Task<List<ProductControlFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductControlInput input);
        Task<ProductControlFullOutPut> UpdateAsync(UpdateProductControlInput input);
        #endregion Async Methods
    }
}