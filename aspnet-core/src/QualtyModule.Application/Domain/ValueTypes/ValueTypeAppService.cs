﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Extensions;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ValueTypes.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.ValueTypes
{
    [AbpAuthorize(PermissionNames.ValueType)]
    public class ValueTypeAppService : QualtyModuleAppServiceBase, IValueTypeAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IValueTypeRepository _valueTypeRepository;

        public ValueTypeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IValueTypeRepository valueTypeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _valueTypeRepository = valueTypeRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ValueType_Create)]
        public async Task<ValueTypeFullOutPut> CreateAsync(CreateValueTypeInput input)
        {
            var valueType = new ValueType()
            {
                Name = input.Name,
            };

            await _valueTypeRepository.InsertAsync(valueType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ValueTypeFullOutPut>(valueType);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ValueType_Get)]
        public async Task<ValueTypeFullOutPut> GetAsync(GetValueTypeInput input)
        {
            var valueType = await _entityManager.GetValueTypeAsync(input.Id);

            return ObjectMapper.Map<ValueTypeFullOutPut>(valueType);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ValueType_GetList)]
        public async Task<List<ValueTypeFullOutPut>> GetListAsync()
        {
            var valueTypeList = await _valueTypeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ValueTypeFullOutPut>>(valueTypeList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ValueType_Delete)]
        public async Task DeleteAsync(DeleteValueTypeInput input)
        {
            var valueType = await _entityManager.GetValueTypeAsync(input.Id);

            await _valueTypeRepository.DeleteAsync(valueType);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ValueType_Update)]
        public async Task<ValueTypeFullOutPut> UpdateAsync(UpdateValueTypeInput input)
        {
            var valueType = await _entityManager.GetValueTypeAsync(input.Id);

            valueType.Name = input.Name.IsNullOrEmpty() ? valueType.Name : input.Name;

            await _valueTypeRepository.UpdateAsync(valueType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ValueTypeFullOutPut>(valueType);
        }
    }
}