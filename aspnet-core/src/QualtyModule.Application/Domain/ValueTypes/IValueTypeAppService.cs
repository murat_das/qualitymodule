﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.ValueTypes.Dtos;

namespace QualtyModule.Domain.ValueTypes
{
    public interface IValueTypeAppService : IApplicationService
    {
        #region Async Methods
        Task<ValueTypeFullOutPut> CreateAsync(CreateValueTypeInput input);
        Task<ValueTypeFullOutPut> GetAsync(GetValueTypeInput input);
        Task<List<ValueTypeFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteValueTypeInput input);
        Task<ValueTypeFullOutPut> UpdateAsync(UpdateValueTypeInput input);
        #endregion Async Methods
    }
}