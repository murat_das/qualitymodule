﻿namespace QualtyModule.Domain.ValueTypes.Dtos
{
    public class CreateValueTypeInput
    {
        public string Name { get; set; }
    }
}