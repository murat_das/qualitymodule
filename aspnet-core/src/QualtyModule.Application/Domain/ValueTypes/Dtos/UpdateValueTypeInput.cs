﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.ValueTypes.Dtos
{
    public class UpdateValueTypeInput:EntityDto<int>
    {
        public string Name { get; set; }
    }
}