﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.TestParameters.Dtos;

namespace QualtyModule.Domain.ValueTypes.Dtos
{
    public class ValueTypeFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public List<TestParameterPartOutPut> TestParameter { get; set; }
    }
}