﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.ValueTypes.Dtos
{
    public class ValueTypePartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}