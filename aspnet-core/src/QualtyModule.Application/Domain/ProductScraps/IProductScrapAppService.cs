﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.ProductScraps.Dtos;

namespace QualtyModule.Domain.ProductScraps
{
    public interface IProductScrapAppService : IApplicationService
    {
        #region Async Methods
        Task<ProductScrapFullOutPut> CreateAsync(CreateProductScrapInput input);
        Task<ProductScrapFullOutPut> GetAsync(GetProductScrapInput input);
        Task<List<ProductScrapFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductScrapInput input);
        Task<ProductScrapFullOutPut> UpdateAsync(UpdateProductScrapInput input);
        #endregion Async Methods
    }
}