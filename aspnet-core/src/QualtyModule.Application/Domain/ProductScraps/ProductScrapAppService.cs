﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ProductScraps.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.ProductScraps
{
    [AbpAuthorize(PermissionNames.ProductScrap)]
    public class ProductScrapAppService:QualtyModuleAppServiceBase,IProductScrapAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductScrapRepository _productScrapRepository;

        public ProductScrapAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductScrapRepository productScrapRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productScrapRepository = productScrapRepository;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductScrap_Create)]
        public async Task<ProductScrapFullOutPut> CreateAsync(CreateProductScrapInput input)
        {
            var productScrap = new ProductScrap()
            {
                ProductId = input.Product?.Id,
                ScrapTypeId = input.ScrapType?.Id
            };

            await _productScrapRepository.InsertAsync(productScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductScrapFullOutPut>(productScrap);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductScrap_Get)]
        public async Task<ProductScrapFullOutPut> GetAsync(GetProductScrapInput input)
        {
            var productScrap=await _entityManager.GetProductScrapAsync(input.Id);

            return ObjectMapper.Map<ProductScrapFullOutPut>(productScrap);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductScrap_GetList)]
        public async Task<List<ProductScrapFullOutPut>> GetListAsync()
        {
            var productScrapList =await _productScrapRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductScrapFullOutPut>>(productScrapList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductScrap_Delete)]
        public async Task DeleteAsync(DeleteProductScrapInput input)
        {
            var productScrap = await _entityManager.GetProductScrapAsync(input.Id);

            await _productScrapRepository.DeleteAsync(productScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ProductScrap_Update)]
        public async Task<ProductScrapFullOutPut> UpdateAsync(UpdateProductScrapInput input)
        {
            var productScrap = await _entityManager.GetProductScrapAsync(input.Id);

            productScrap.ProductId = input.Product?.Id;
            productScrap.ScrapTypeId = input.ScrapType?.Id;

            await _productScrapRepository.UpdateAsync(productScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductScrapFullOutPut>(productScrap);
        }
    }
}