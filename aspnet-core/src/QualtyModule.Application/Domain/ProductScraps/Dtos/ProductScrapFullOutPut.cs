﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.Scraps.Dtos;
using QualtyModule.Domain.ScrapTypes.Dtos;

namespace QualtyModule.Domain.ProductScraps.Dtos
{
    public class ProductScrapFullOutPut:EntityDto<int>
    {
        public ProductPartOutPut Product { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }
        public List<ScrapPartOutPut> Scraps { get; set; }
    }
}