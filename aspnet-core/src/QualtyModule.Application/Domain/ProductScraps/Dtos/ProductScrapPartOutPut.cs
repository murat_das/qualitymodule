﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.ScrapTypes.Dtos;

namespace QualtyModule.Domain.ProductScraps.Dtos
{
    public class ProductScrapPartOutPut:EntityDto<int>
    {
        public ProductPartOutPut Product { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }
    }
}