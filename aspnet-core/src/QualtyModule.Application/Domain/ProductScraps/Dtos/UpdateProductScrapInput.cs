﻿using Abp.Application.Services.Dto;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.ScrapTypes.Dtos;

namespace QualtyModule.Domain.ProductScraps.Dtos
{
    public class UpdateProductScrapInput:EntityDto<int>
    {
        public ProductPartOutPut Product { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }

    }
}