﻿using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.ScrapTypes.Dtos;

namespace QualtyModule.Domain.ProductScraps.Dtos
{
    public class CreateProductScrapInput
    {
        public ProductPartOutPut Product { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }
    }
}