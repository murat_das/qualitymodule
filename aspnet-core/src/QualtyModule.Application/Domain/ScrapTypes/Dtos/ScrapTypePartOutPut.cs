﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Domain.ScrapTypes.Dtos
{
    public class ScrapTypePartOutPut:EntityDto<int>
    {
        public string Name { get; set; }
    }
}