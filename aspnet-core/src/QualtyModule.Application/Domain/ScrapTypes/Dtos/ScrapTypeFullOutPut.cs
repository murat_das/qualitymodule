﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using QualtyModule.Domain.ProductScraps.Dtos;

namespace QualtyModule.Domain.ScrapTypes.Dtos
{
    public class ScrapTypeFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public List<ProductScrapPartOutPut> ProductScraps { get; set; }
    }
}