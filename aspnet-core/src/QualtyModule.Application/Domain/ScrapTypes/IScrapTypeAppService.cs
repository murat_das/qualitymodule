﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Domain.ScrapTypes.Dtos;

namespace QualtyModule.Domain.ScrapTypes
{
    public interface IScrapTypeAppService : IApplicationService
    {
        #region Async Methods
        Task<ScrapTypeFullOutPut> CreateAsync(CreateScrapTypeInput input);
        Task<ScrapTypeFullOutPut> GetAsync(GetScrapTypeInput input);
        Task<List<ScrapTypeFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteScrapTypeInput input);
        Task<ScrapTypeFullOutPut> UpdateAsync(UpdateScrapTypeInput input);
        #endregion Async Methods
    }
}