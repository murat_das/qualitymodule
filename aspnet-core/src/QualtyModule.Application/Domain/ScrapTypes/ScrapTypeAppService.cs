﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Mvc;
using QualtyModule.Authorization;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ScrapTypes.Dtos;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Manager;

namespace QualtyModule.Domain.ScrapTypes
{
    [AbpAuthorize(PermissionNames.ScrapType)]
    public class ScrapTypeAppService:QualtyModuleAppServiceBase,IScrapTypeAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IScrapTypeRepository _scrapTypeRepository;

        public ScrapTypeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IScrapTypeRepository scrapTypeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _scrapTypeRepository = scrapTypeRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.ScrapType_Create)]
        public async Task<ScrapTypeFullOutPut> CreateAsync(CreateScrapTypeInput input)
        {
            var scrapType = new ScrapType()
            {
                Name = input.Name,
            };

            await _scrapTypeRepository.InsertAsync(scrapType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapTypeFullOutPut>(scrapType);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ScrapType_Get)]
        public async Task<ScrapTypeFullOutPut> GetAsync(GetScrapTypeInput input)
        {
            var scrapType = await _entityManager.GetScrapTypeAsync(input.Id);

            return ObjectMapper.Map<ScrapTypeFullOutPut>(scrapType);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ScrapType_GetList)]
        public async Task<List<ScrapTypeFullOutPut>> GetListAsync()
        {
            var scrapTypeList = await _scrapTypeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ScrapTypeFullOutPut>>(scrapTypeList);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ScrapType_Delete)]
        public async Task DeleteAsync(DeleteScrapTypeInput input)
        {
            var scrapType = await _entityManager.GetScrapTypeAsync(input.Id);

            await _scrapTypeRepository.DeleteAsync(scrapType);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ScrapType_Update)]
        public async Task<ScrapTypeFullOutPut> UpdateAsync(UpdateScrapTypeInput input)
        {
            var scrapType = await _entityManager.GetScrapTypeAsync(input.Id);

            scrapType.Name = input.Name.IsNullOrEmpty() ? scrapType.Name : input.Name;

            await _scrapTypeRepository.UpdateAsync(scrapType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapTypeFullOutPut>(scrapType);
        }
    }
}