using System.ComponentModel.DataAnnotations;

namespace QualtyModule.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}