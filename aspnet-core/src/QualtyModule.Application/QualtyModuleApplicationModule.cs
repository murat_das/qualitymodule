﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using QualtyModule.Authorization;
using QualtyModule.Manager;

namespace QualtyModule
{
    [DependsOn(
        typeof(QualtyModuleCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class QualtyModuleApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<QualtyModuleAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(QualtyModuleApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
                {
                    MapperManager.DtosToDomain(cfg);
                    // Scan the assembly for classes which inherit from AutoMapper.Profile
                    cfg.AddMaps(thisAssembly);
                }
            );
        }
    }
}
