﻿using Abp.Application.Services.Dto;

namespace QualtyModule.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

