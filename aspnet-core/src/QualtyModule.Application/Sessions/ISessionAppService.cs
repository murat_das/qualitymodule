﻿using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Sessions.Dto;

namespace QualtyModule.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
