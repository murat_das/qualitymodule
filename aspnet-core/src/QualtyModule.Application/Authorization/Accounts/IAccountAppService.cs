﻿using System.Threading.Tasks;
using Abp.Application.Services;
using QualtyModule.Authorization.Accounts.Dto;

namespace QualtyModule.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
