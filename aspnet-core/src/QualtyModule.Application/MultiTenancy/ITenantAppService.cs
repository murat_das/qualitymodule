﻿using Abp.Application.Services;
using QualtyModule.MultiTenancy.Dto;

namespace QualtyModule.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

