﻿using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Castle.Core.Logging;
using QualtyModule.Domain.Entities;
using QualtyModule.EntityFrameworkCore.Repositories;
using QualtyModule.Services;

namespace QualtyModule.Manager
{
    public class EntityManager:AbpAppServiceBase,IEntityManager
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductControlRepository _productControlRepository;
        private readonly IProductControlTestRepository _productControlTestRepository;
        private readonly IProductScrapRepository _productScrapRepository;
        private readonly IProductTestRepository _productTestRepository;
        private readonly IRuleRepository _ruleRepository;
        private readonly IScrapRepository _scrapRepository;
        private readonly IScrapTypeRepository _scrapTypeRepository;
        private readonly ITestRepository _testRepository;
        private readonly ITestParameterRepository _testParameterRepository;
        private readonly ITestResultRepository _testResultRepository;
        private readonly IUnitRepository _unitRepository;
        private readonly IValueTypeRepository _valueTypeRepository;



        public EntityManager(IAbpSession abpSession,
            ILocalizationManager localizationManager,
            ILogger logger,
            IUnitOfWorkManager unitOfWorkManager,
            IObjectMapper objectMapper,
            IProductRepository productRepository,
            IProductControlRepository productControlRepository,
            IProductControlTestRepository productControlTestRepository,
            IProductScrapRepository productScrapRepository,
            IProductTestRepository productTestRepository,
            IRuleRepository ruleRepository,
            IScrapRepository scrapRepository,
            IScrapTypeRepository scrapTypeRepository,
            ITestRepository testRepository,
            ITestParameterRepository testParameterRepository,
            ITestResultRepository testResultRepository,
            IUnitRepository unitRepository,
            IValueTypeRepository valueTypeRepository) 
            : base(abpSession, localizationManager, logger, unitOfWorkManager, objectMapper)
        {
            _productRepository = productRepository;
            _productControlTestRepository = productControlTestRepository;
            _productScrapRepository = productScrapRepository;
            _productTestRepository = productTestRepository;
            _ruleRepository = ruleRepository;
            _scrapRepository = scrapRepository;
            _scrapTypeRepository = scrapTypeRepository;
            _testRepository = testRepository;
            _testResultRepository = testResultRepository;
            _unitRepository = unitRepository;
            _valueTypeRepository = valueTypeRepository;
            _testParameterRepository = testParameterRepository;
            _productControlRepository = productControlRepository;
        }

        public async Task<Product> GetProductAsync(int productId)
        {
            var product = await _productRepository.FirstOrDefaultAsync(x =>
                    x.Id == productId
            );
            if (product == null)
            {
                throw new EntityNotFoundException(typeof(Product), productId);
            }

            return product;
        }

        public async Task<ProductControl> GetProductControlAsync(int productControlId)
        {
            var productControl = await _productControlRepository.FirstOrDefaultAsync(x =>
                x.Id == productControlId
            );
            if (productControl == null)
            {
                throw new EntityNotFoundException(typeof(ProductControl), productControlId);
            }

            return productControl;
        }

        public async Task<ProductControlTest> GetProductControlTestAsync(int productControlTestId)
        {
            var productControlTest = await _productControlTestRepository.FirstOrDefaultAsync(x =>
                x.Id == productControlTestId
            );
            if (productControlTest == null)
            {
                throw new EntityNotFoundException(typeof(ProductControlTest), productControlTestId);
            }

            return productControlTest;
        }

        public async Task<ProductScrap> GetProductScrapAsync(int productScrapId)
        {
            var productScrap = await _productScrapRepository.FirstOrDefaultAsync(x =>
                x.Id == productScrapId
            );
            if (productScrap == null)
            {
                throw new EntityNotFoundException(typeof(ProductScrap), productScrapId);
            }

            return productScrap;
        }

        public async Task<ProductTest> GetProductTestAsync(int productTestId)
        {
            var productTest = await _productTestRepository.FirstOrDefaultAsync(x =>
                x.Id == productTestId
            );
            if (productTest == null)
            {
                throw new EntityNotFoundException(typeof(ProductTest), productTestId);
            }

            return productTest;
        }

        public async Task<Rule> GetRuleAsync(int ruleId)
        {
            var rule = await _ruleRepository.FirstOrDefaultAsync(x =>
                x.Id == ruleId
            );
            if (rule == null)
            {
                throw new EntityNotFoundException(typeof(Rule), ruleId);
            }

            return rule;
        }

        public async Task<Scrap> GetScrapAsync(int scrapId)
        {
            var scrap = await _scrapRepository.FirstOrDefaultAsync(x =>
                x.Id == scrapId
            );
            if (scrap == null)
            {
                throw new EntityNotFoundException(typeof(Scrap), scrapId);
            }

            return scrap;
        }

        public async Task<ScrapType> GetScrapTypeAsync(int scrapTypeId)
        {
            var scrapType = await _scrapTypeRepository.FirstOrDefaultAsync(x =>
                x.Id == scrapTypeId
            );
            if (scrapType == null)
            {
                throw new EntityNotFoundException(typeof(ScrapType), scrapTypeId);
            }

            return scrapType;
        }

        public async Task<Test> GetTestAsync(int testId)
        {
            var test = await _testRepository.FirstOrDefaultAsync(x =>
                x.Id == testId
            );
            if (test == null)
            {
                throw new EntityNotFoundException(typeof(Test), testId);
            }

            return test;
        }

        public async Task<TestParameter> GetTestParameterAsync(int testParameterId)
        {
            var testParameter = await _testParameterRepository.FirstOrDefaultAsync(x =>
                x.Id == testParameterId
            );
            if (testParameter == null)
            {
                throw new EntityNotFoundException(typeof(TestParameter), testParameterId);
            }

            return testParameter;
        }

        public async Task<TestResult> GetTestResultAsync(int testResultId)
        {
            var testResult = await _testResultRepository.FirstOrDefaultAsync(x =>
                x.Id == testResultId
            );
            if (testResult == null)
            {
                throw new EntityNotFoundException(typeof(TestResult), testResultId);
            }

            return testResult;
        }

        public async Task<Unit> GetUnitAsync(int unitId)
        {
            var unit = await _unitRepository.FirstOrDefaultAsync(x =>
                x.Id == unitId
            );
            if (unit == null)
            {
                throw new EntityNotFoundException(typeof(Unit), unitId);
            }

            return unit;
        }

        public async Task<ValueType> GetValueTypeAsync(int valueTypeId)
        {
            var valueType = await _valueTypeRepository.FirstOrDefaultAsync(x =>
                x.Id == valueTypeId
            );
            if (valueType == null)
            {
                throw new EntityNotFoundException(typeof(ValueType), valueTypeId);
            }

            return valueType;
        }
    }
}