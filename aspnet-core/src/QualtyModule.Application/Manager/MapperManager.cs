﻿using AutoMapper;
using QualtyModule.Domain.Entities;
using QualtyModule.Domain.ProductControls.Dtos;
using QualtyModule.Domain.ProductControlTests.Dtos;
using QualtyModule.Domain.Products.Dtos;
using QualtyModule.Domain.ProductScraps.Dtos;
using QualtyModule.Domain.ProductTests.Dtos;
using QualtyModule.Domain.Rules.Dtos;
using QualtyModule.Domain.Scraps.Dtos;
using QualtyModule.Domain.ScrapTypes.Dtos;
using QualtyModule.Domain.TestParameters.Dtos;
using QualtyModule.Domain.TestResults.Dtos;
using QualtyModule.Domain.Tests.Dtos;
using QualtyModule.Domain.Units.Dtos;
using QualtyModule.Domain.ValueTypes.Dtos;

namespace QualtyModule.Manager
{
    public class MapperManager
    {
        public static void DtosToDomain(IMapperConfigurationExpression cfg) //Dtolu verileri Entitylere göre çevir
        {
            #region Products
            cfg.CreateMap<Product, CreateProductInput>();
            cfg.CreateMap<Product, GetProductInput>();
            cfg.CreateMap<Product, DeleteProductInput>();
            cfg.CreateMap<Product, UpdateProductInput>();
            cfg.CreateMap<Product, ProductFullOutPut>();
            cfg.CreateMap<Product, ProductPartOutPut>();
            #endregion

            #region ProductControls
            cfg.CreateMap<ProductControl, CreateProductControlInput>();
            cfg.CreateMap<ProductControl, GetProductControlInput>();
            cfg.CreateMap<ProductControl, DeleteProductControlInput>();
            cfg.CreateMap<ProductControl, UpdateProductControlInput>();
            cfg.CreateMap<ProductControl, ProductControlFullOutPut>();
            cfg.CreateMap<ProductControl, ProductControlPartOutPut>();
            #endregion

            #region ProductControlTests
            cfg.CreateMap<ProductControlTest, CreateProductControlTestInput>();
            cfg.CreateMap<ProductControlTest, GetProductControlTestInput>();
            cfg.CreateMap<ProductControlTest, DeleteProductControlTestInput>();
            cfg.CreateMap<ProductControlTest, UpdateProductControlTestInput>();
            cfg.CreateMap<ProductControlTest, ProductControlTestFullOutPut>();
            cfg.CreateMap<ProductControlTest, ProductControlTestPartOutPut>();
            #endregion

            #region ProductTests
            cfg.CreateMap<ProductTest, CreateProductTestInput>();
            cfg.CreateMap<ProductTest, GetProductTestInput>();
            cfg.CreateMap<ProductTest, DeleteProductTestInput>();
            cfg.CreateMap<ProductTest, UpdateProductTestInput>();
            cfg.CreateMap<ProductTest, ProductTestFullOutPut>();
            cfg.CreateMap<ProductTest, ProductTestPartOutPut>();
            #endregion

            #region ProductScraps
            cfg.CreateMap<ProductScrap, CreateProductScrapInput>();
            cfg.CreateMap<ProductScrap, GetProductScrapInput>();
            cfg.CreateMap<ProductScrap, DeleteProductScrapInput>();
            cfg.CreateMap<ProductScrap, UpdateProductScrapInput>();
            cfg.CreateMap<ProductScrap, ProductScrapFullOutPut>();
            cfg.CreateMap<ProductScrap, ProductScrapPartOutPut>();
            #endregion

            #region Rules
            cfg.CreateMap<Rule, CreateRuleInput>();
            cfg.CreateMap<Rule, GetRuleInput>();
            cfg.CreateMap<Rule, DeleteRuleInput>();
            cfg.CreateMap<Rule, UpdateRuleInput>();
            cfg.CreateMap<Rule, RuleFullOutPut>();
            cfg.CreateMap<Rule, RulePartOutPut>();
            #endregion

            #region Scraps
            cfg.CreateMap<Scrap, CreateScrapInput>();
            cfg.CreateMap<Scrap, GetScrapInput>();
            cfg.CreateMap<Scrap, DeleteScrapInput>();
            cfg.CreateMap<Scrap, UpdateScrapInput>();
            cfg.CreateMap<Scrap, ScrapFullOutPut>();
            cfg.CreateMap<Scrap, ScrapPartOutPut>();
            #endregion

            #region ScrapTypes
            cfg.CreateMap<ScrapType, CreateScrapTypeInput>();
            cfg.CreateMap<ScrapType, GetScrapTypeInput>();
            cfg.CreateMap<ScrapType, DeleteScrapTypeInput>();
            cfg.CreateMap<ScrapType, UpdateScrapTypeInput>();
            cfg.CreateMap<ScrapType, ScrapTypeFullOutPut>();
            cfg.CreateMap<ScrapType, ScrapTypePartOutPut>();
            #endregion

            #region Tests
            cfg.CreateMap<Test, CreateTestInput>();
            cfg.CreateMap<Test, GetTestInput>();
            cfg.CreateMap<Test, DeleteTestInput>();
            cfg.CreateMap<Test, UpdateTestInput>();
            cfg.CreateMap<Test, TestFullOutPut>();
            cfg.CreateMap<Test, TestPartOutPut>();
            #endregion

            #region TestParameters
            cfg.CreateMap<TestParameter, CreateTestParameterInput>();
            cfg.CreateMap<TestParameter, GetTestParameterInput>();
            cfg.CreateMap<TestParameter, DeleteTestParameterInput>();
            cfg.CreateMap<TestParameter, UpdateTestParameterInput>();
            cfg.CreateMap<TestParameter, TestParameterFullOutPut>();
            cfg.CreateMap<TestParameter, TestParameterPartOutPut>();
            #endregion

            #region TestResults
            cfg.CreateMap<TestResult, CreateTestResultInput>();
            cfg.CreateMap<TestResult, GetTestResultInput>();
            cfg.CreateMap<TestResult, DeleteTestResultInput>();
            cfg.CreateMap<TestResult, UpdateTestResultInput>();
            cfg.CreateMap<TestResult, TestResultFullOutPut>();
            cfg.CreateMap<TestResult, TestResultPartOutPut>();
            #endregion

            #region Units
            cfg.CreateMap<Unit, CreateUnitInput>();
            cfg.CreateMap<Unit, GetUnitInput>();
            cfg.CreateMap<Unit, DeleteUnitInput>();
            cfg.CreateMap<Unit, UpdateUnitInput>();
            cfg.CreateMap<Unit, UnitFullOutPut>();
            cfg.CreateMap<Unit, UnitPartOutPut>();
            #endregion

            #region ValueTypes
            cfg.CreateMap<ValueType, CreateValueTypeInput>();
            cfg.CreateMap<ValueType, GetValueTypeInput>();
            cfg.CreateMap<ValueType, DeleteValueTypeInput>();
            cfg.CreateMap<ValueType, UpdateValueTypeInput>();
            cfg.CreateMap<ValueType, ValueTypeFullOutPut>();
            cfg.CreateMap<ValueType, ValueTypePartOutPut>();
            #endregion
        }
    }
}