﻿using System.Threading.Tasks;
using Abp.Dependency;
using QualtyModule.Domain.Entities;

namespace QualtyModule.Manager
{
    public interface IEntityManager : ITransientDependency
    {
        Task<Product> GetProductAsync(int productId);
        Task<ProductControl> GetProductControlAsync(int productControlId);
        Task<ProductControlTest> GetProductControlTestAsync(int productControlTestId);
        Task<ProductScrap> GetProductScrapAsync(int productScrapId);
        Task<ProductTest> GetProductTestAsync(int productTestId);
        Task<Rule> GetRuleAsync(int ruleId);
        Task<Scrap> GetScrapAsync(int scrapId);
        Task<ScrapType> GetScrapTypeAsync(int scrapTypeId);
        Task<Test> GetTestAsync(int testId);
        Task<TestParameter> GetTestParameterAsync(int testParameterId);
        Task<TestResult> GetTestResultAsync(int testResultId);
        Task<Unit> GetUnitAsync(int unitId);
        Task<ValueType> GetValueTypeAsync(int valueTypeId);

    }
}