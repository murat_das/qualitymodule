import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientJsonpModule } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";

import { ModalModule } from "ngx-bootstrap/modal";
import { NgxPaginationModule } from "ngx-pagination";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { AbpModule } from "@abp/abp.module";

import { ServiceProxyModule } from "@shared/service-proxies/service-proxy.module";
import { SharedModule } from "@shared/shared.module";

import { HomeComponent } from "@app/home/home.component";
import { AboutComponent } from "@app/about/about.component";
import { TopBarComponent } from "@app/layout/topbar.component";
import { TopBarLanguageSwitchComponent } from "@app/layout/topbar-languageswitch.component";
import { SideBarUserAreaComponent } from "@app/layout/sidebar-user-area.component";
import { SideBarNavComponent } from "@app/layout/sidebar-nav.component";
import { SideBarFooterComponent } from "@app/layout/sidebar-footer.component";
import { RightSideBarComponent } from "@app/layout/right-sidebar.component";
// tenants
import { TenantsComponent } from "@app/tenants/tenants.component";
import { CreateTenantDialogComponent } from "./tenants/create-tenant/create-tenant-dialog.component";
import { EditTenantDialogComponent } from "./tenants/edit-tenant/edit-tenant-dialog.component";
// roles
import { RolesComponent } from "@app/roles/roles.component";
import { CreateRoleDialogComponent } from "./roles/create-role/create-role-dialog.component";
import { EditRoleDialogComponent } from "./roles/edit-role/edit-role-dialog.component";
// users
import { UsersComponent } from "@app/users/users.component";
import { CreateUserDialogComponent } from "@app/users/create-user/create-user-dialog.component";
import { EditUserDialogComponent } from "@app/users/edit-user/edit-user-dialog.component";
import { ChangePasswordComponent } from "./users/change-password/change-password.component";
import { ResetPasswordDialogComponent } from "./users/reset-password/reset-password.component";
import { DevextremeComponentModule } from "./devextreme-component.module";
import { QualityModuleServiceModule } from "./services/qualityModule-service.module";
import { ProductComponent } from "./components/product/product.component";
import { CreateProductDialogComponent } from "./components/product/create-product/create-product-dialog.component";
import { EditProductDialogComponent } from "./components/product/edit-product/edit-product-dialog.component";
import { CreateScrapTypeDialogComponent } from "./components/scrapType/create-scrapType/create-scrapType-dialog.component";
import { ScrapTypeComponent } from "./components/scrapType/scrapType.component";
import { EditScrapTypeDialogComponent } from "./components/scrapType/edit-scrapType/edit-scrapType-dialog.component";
import { CreateTestDialogComponent } from "./components/test/create-test/create-test-dialog.component";
import { EditTestDialogComponent } from "./components/test/edit-test/edit-test-dialog.component";
import { TestComponent } from "./components/test/test.component";
import { UnitComponent } from "./components/unit/unit.component";
import { CreateUnitDialogComponent } from "./components/unit/create-unit/create-unit-dialog.component";
import { EditUnitDialogComponent } from "./components/unit/edit-unit/edit-unit-dialog.component";
import { CreateValueTypeDialogComponent } from "./components/valueType/create-valueType/create-valueType-dialog.component";
import { EditValueTypeDialogComponent } from "./components/valueType/edit-valueType/edit-valueType-dialog.component";
import { ValueTypeComponent } from "./components/valueType/valueType.component";
import { EditRuleDialogComponent } from "./components/rule/edit-rule/edit-rule-dialog.component";
import { RuleComponent } from "./components/rule/rule.component";
import { CreateRuleDialogComponent } from "./components/rule/create-rule/create-rule-dialog.component";
import { ProductTestComponent } from "./components/productTest/productTest.component";
import { EditProductTestDialogComponent } from "./components/productTest/edit-productTest/edit-productTest-dialog.component";
import { CreateProductTestDialogComponent } from "./components/productTest/create-productTest/create-productTest-dialog.component";
import { ProductScrapComponent } from "./components/productScrap/productScrap.component";
import { CreateProductScrapDialogComponent } from "./components/productScrap/create-productScrap/create-productScrap-dialog.component";
import { EditProductScrapDialogComponent } from "./components/productScrap/edit-productScrap/edit-productScrap-dialog.component";
import { ProductControlComponent } from "./components/productControl/productControl.component";
import { CreateProductControlDialogComponent } from "./components/productControl/create-productControl/create-productControl-dialog.component";
import { EditProductControlDialogComponent } from "./components/productControl/edit-productControl/edit-productControl-dialog.component";
import { ScrapComponent } from "./components/scrap/scrap.component";
import { CreateScrapDialogComponent } from "./components/scrap/create-scrap/create-scrap-dialog.component";
import { EditScrapDialogComponent } from "./components/scrap/edit-scrap/edit-scrap-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    TopBarComponent,
    TopBarLanguageSwitchComponent,
    SideBarUserAreaComponent,
    SideBarNavComponent,
    SideBarFooterComponent,
    RightSideBarComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // products
    ProductComponent,
    CreateProductDialogComponent,
    EditProductDialogComponent,
    // scrapTypes
    ScrapTypeComponent,
    CreateScrapTypeDialogComponent,
    EditScrapTypeDialogComponent,
    // tests
    TestComponent,
    CreateTestDialogComponent,
    EditTestDialogComponent,
    // units
    UnitComponent,
    CreateUnitDialogComponent,
    EditUnitDialogComponent,
    //valueTypes
    ValueTypeComponent,
    CreateValueTypeDialogComponent,
    EditValueTypeDialogComponent,
    //rules
    RuleComponent,
    CreateRuleDialogComponent,
    EditRuleDialogComponent,
    //productTests
    ProductTestComponent,
    CreateProductTestDialogComponent,
    EditProductTestDialogComponent,
    //productScraps
    ProductScrapComponent,
    CreateProductScrapDialogComponent,
    EditProductScrapDialogComponent,
    // ProductControls
    ProductControlComponent,
    CreateProductControlDialogComponent,
    EditProductControlDialogComponent,
    // Scraps
    ScrapComponent,
    CreateScrapDialogComponent,
    EditScrapDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    QualityModuleServiceModule,
    DevextremeComponentModule,
  ],
  providers: [],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    // products
    CreateProductDialogComponent,
    EditProductDialogComponent,
    // scrapTypes
    CreateScrapTypeDialogComponent,
    EditScrapTypeDialogComponent,
    // tests
    CreateTestDialogComponent,
    EditTestDialogComponent,
    //units
    CreateUnitDialogComponent,
    EditUnitDialogComponent,
    //valueTypes
    CreateValueTypeDialogComponent,
    EditValueTypeDialogComponent,
    //rules
    CreateRuleDialogComponent,
    EditRuleDialogComponent,
    //productTests
    CreateProductTestDialogComponent,
    EditProductTestDialogComponent,
    //productScraps
    CreateProductScrapDialogComponent,
    EditProductScrapDialogComponent,
     // ProductControls
     CreateProductControlDialogComponent,
     EditProductControlDialogComponent,
     // Scraps
     CreateScrapDialogComponent,
     EditScrapDialogComponent,
  ],
})
export class AppModule {}
