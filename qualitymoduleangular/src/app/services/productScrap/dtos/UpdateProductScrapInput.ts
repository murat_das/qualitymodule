import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";

export class UpdateProductScrapInput implements IUpdateProductScrapInput {
  id: number;
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}

export interface IUpdateProductScrapInput {
  id: number;
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}
