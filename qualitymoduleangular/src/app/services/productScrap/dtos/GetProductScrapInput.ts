export class GetProductScrapInput implements IGetProductScrapInput {
  id: number;
}

export interface IGetProductScrapInput {
  id: number;
}
