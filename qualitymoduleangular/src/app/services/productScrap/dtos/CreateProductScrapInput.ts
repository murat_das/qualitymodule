import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";

export class CreateProductScrapInput implements ICreateProductScrapInput {
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}

export interface ICreateProductScrapInput {
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}
