import { isObject } from "util";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";

export class ProductScrapFullOutPut implements IProductScrapFullOutPut {
  id: number;
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;

  constructor(data?: IProductScrapFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.product = data.product;
        this.scrapType = data.scrapType;
      }
    }
  }

  static fromJS(data: any): ProductScrapFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ProductScrapFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["product"] = this.product;
    data["scrapType"] = this.scrapType;
    return data;
  }

  clone(): ProductScrapFullOutPut {
    const json = this.toJSON();
    let result = new ProductScrapFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IProductScrapFullOutPut {
  id: number;
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}
