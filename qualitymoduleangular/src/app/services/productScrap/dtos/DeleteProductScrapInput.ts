import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";

export class DeleteProductScrapInput implements IDeleteProductScrapInput {
  id: number;
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}

export interface IDeleteProductScrapInput {
  id: number;
  product: ProductFullOutPut;
  scrapType: ScrapTypeFullOutPut;
}
