/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductScrapService } from './productScrap.service';

describe('Service: ProductScrap', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductScrapService]
    });
  });

  it('should ...', inject([ProductScrapService], (service: ProductScrapService) => {
    expect(service).toBeTruthy();
  }));
});
