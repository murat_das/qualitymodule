import { isObject } from "util";
import { ProductScrapFullOutPut } from "@app/services/productScrap/dtos/ProductScrapFullOutPut";
import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";

export class ScrapFullOutPut implements IScrapFullOutPut {
  id: number;
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
  amount: number;

  constructor(data?: IScrapFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.productScrap = data.productScrap;
        this.productControl = data.productControl;
        this.amount = data.amount;
      }
    }
  }

  static fromJS(data: any): ScrapFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ScrapFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["productScrap"] = this.productScrap;
    data["productControl"] = this.productControl;
    data["amount"] = this.amount;
    return data;
  }

  clone(): ScrapFullOutPut {
    const json = this.toJSON();
    let result = new ScrapFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IScrapFullOutPut {
    id: number;
    productScrap: ProductScrapFullOutPut;
    productControl: ProductControlFullOutPut;
    amount:number;
}
