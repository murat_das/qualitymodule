export class ScrapPartOutPut implements IScrapPartOutPut {
  id: number;
  amount: number;
}

export interface IScrapPartOutPut {
  id: number;
  amount: number;
}
