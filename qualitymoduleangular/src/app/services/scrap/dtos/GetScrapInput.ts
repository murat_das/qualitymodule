export class GetScrapInput implements IGetScrapInput {
  id: number;
}

export interface IGetScrapInput {
  id: number;
}
