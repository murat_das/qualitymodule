import { ProductScrapFullOutPut } from "@app/services/productScrap/dtos/ProductScrapFullOutPut";
import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";

export class CreateScrapInput implements ICreateScrapInput {
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
  amount: number;
}

export interface ICreateScrapInput {
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
  amount: number;
}
