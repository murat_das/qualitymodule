import { ProductScrapFullOutPut } from "@app/services/productScrap/dtos/ProductScrapFullOutPut";
import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";

export class DeleteScrapInput implements IDeleteScrapInput {
  id: number;
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
}

export interface IDeleteScrapInput {
  id: number;
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
}
