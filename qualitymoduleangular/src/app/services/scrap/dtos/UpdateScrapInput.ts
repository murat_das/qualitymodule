import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";
import { ProductScrapFullOutPut } from "@app/services/productScrap/dtos/ProductScrapFullOutPut";

export class UpdateScrapInput implements IUpdateScrapInput {
  id: number;
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
  amount: number;
}

export interface IUpdateScrapInput {
  id: number;
  productScrap: ProductScrapFullOutPut;
  productControl: ProductControlFullOutPut;
  amount: number;
}
