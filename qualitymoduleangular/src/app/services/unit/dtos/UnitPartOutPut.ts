export class UnitPartOutPut implements IUnitPartOutPut {
  id: number;
  name: string;
}

export interface IUnitPartOutPut {
  id: number;
  name: string;
}
