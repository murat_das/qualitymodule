export class UpdateUnitInput implements IUpdateUnitInput {
  id: number;
  name: string;
}

export interface IUpdateUnitInput {
  id: number;
  name: string;
}
