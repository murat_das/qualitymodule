export class DeleteUnitInput implements IDeleteUnitInput{
    id: number;
    name: string;
}

export interface IDeleteUnitInput{
    id:number;
    name:string;
}
