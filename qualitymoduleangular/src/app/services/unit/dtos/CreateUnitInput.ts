export class CreateUnitInput implements ICreateUnitInput {
  name: string;
}

export interface ICreateUnitInput {
  name: string;
}
