import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";

export class CreateProductTestInput implements ICreateProductTestInput {
  product: ProductFullOutPut;
  test: TestFullOutPut;
}

export interface ICreateProductTestInput {
  product: ProductFullOutPut;
  test: TestFullOutPut;
}
