import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";

export class UpdateProductTestInput implements IUpdateProductTestInput{
    id: number;
    product: ProductFullOutPut;
    test: TestFullOutPut;
}

export interface IUpdateProductTestInput{
    id:number;
    product:ProductFullOutPut;
    test:TestFullOutPut;
}
