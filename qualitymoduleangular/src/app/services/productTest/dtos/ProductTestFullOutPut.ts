import { isObject } from "util";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";

export class ProductTestFullOutPut implements IProductTestFullOutPut {
  id: number;
  product: ProductFullOutPut;
  test: TestFullOutPut;

  constructor(data?: IProductTestFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.product = data.product;
        this.test = data.test;
      }
    }
  }

  static fromJS(data: any): ProductTestFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ProductTestFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["product"] = this.product;
    data["test"] = this.test;
    return data;
  }

  clone(): ProductTestFullOutPut {
    const json = this.toJSON();
    let result = new ProductTestFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IProductTestFullOutPut {
  id: number;
  product: ProductFullOutPut;
  test: TestFullOutPut;
}
