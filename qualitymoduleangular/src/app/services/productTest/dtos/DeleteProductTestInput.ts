import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";

export class DeleteProductTestInput implements IDeleteProductTestInput {
  product: ProductFullOutPut;
  test: TestFullOutPut;
  id: number;
}

export interface IDeleteProductTestInput {
  id: number;
  product:ProductFullOutPut;
  test:TestFullOutPut;
}
