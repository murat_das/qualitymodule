export class GetProductTestInput implements IGetProductTestInput {
  id: number;
}

export interface IGetProductTestInput {
  id: number;
}
