export class DeleteValueTypeInput implements IDeleteValueTypeInput {
  id: number;
  name: string;
}

export interface IDeleteValueTypeInput {
  id: number;
  name: string;
}
