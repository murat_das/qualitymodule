import { isObject } from "util";

export class ValueTypeFullOutPut implements IValueTypeFullOutPut {
  id: number;
  name: string;

  constructor(data?: IValueTypeFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.name = data.name;
      }
    }
  }

  static fromJS(data: any): ValueTypeFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ValueTypeFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["name"] = this.name;
    return data;
  }

  clone(): ValueTypeFullOutPut {
    const json = this.toJSON();
    let result = new ValueTypeFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IValueTypeFullOutPut {
  id: number;
  name: string;
}
