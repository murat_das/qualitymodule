export class CreateValueTypeInput implements ICreateValueTypeInput {
  name: string;
}

export interface ICreateValueTypeInput {
  name: string;
}
