export class ValueTypePartOutPut implements IValueTypePartOutPut {
  id: number;
  name: string;
}

export interface IValueTypePartOutPut {
  id: number;
  name: string;
}
