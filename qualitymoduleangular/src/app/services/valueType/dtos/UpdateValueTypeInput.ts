export class UpdateValueTypeInput implements IUpdateValueTypeInput {
  id: number;
  name: string;
}

export interface IUpdateValueTypeInput {
  id: number;
  name: string;
}
