export class GetValueTypeInput implements IGetValueTypeInput {
  id: number;
}

export interface IGetValueTypeInput {
  id: number;
}
