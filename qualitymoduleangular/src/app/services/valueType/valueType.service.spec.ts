/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ValueTypeService } from './valueType.service';

describe('Service: ValueType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValueTypeService]
    });
  });

  it('should ...', inject([ValueTypeService], (service: ValueTypeService) => {
    expect(service).toBeTruthy();
  }));
});
