import { isObject } from "util";

export class RuleFullOutPut implements IRuleFullOutPut {
  id: number;
  name: string;
  min: number;
  max: number;

  constructor(data?: IRuleFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.name = data.name;
        this.min = data.min;
        this.max = data.max;
      }
    }
  }

  static fromJS(data: any): RuleFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new RuleFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["name"] = this.name;
    data["min"] = this.min;
    data["max"] = this.max;

    return data;
  }

  clone(): RuleFullOutPut {
    const json = this.toJSON();
    let result = new RuleFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IRuleFullOutPut {
  id: number;
  name: string;
  min: number;
  max: number;
}
