export class GetRuleInput implements IGetRuleInput {
  id: number;
}

export interface IGetRuleInput {
  id: number;
}
