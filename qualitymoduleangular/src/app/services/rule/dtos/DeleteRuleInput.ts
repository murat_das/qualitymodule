export class DeleteRuleInput implements IDeleteRuleInput {
  id: number;
  name: string;
}

export interface IDeleteRuleInput {
  id: number;
  name: string;
}
