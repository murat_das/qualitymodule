export class UpdateRuleInput implements IUpdateRuleInput {
  id: number;
  name: string;
  min: number;
  max: number;
}

export interface IUpdateRuleInput {
  id: number;
  name: string;
  min: number;
  max: number;
}
