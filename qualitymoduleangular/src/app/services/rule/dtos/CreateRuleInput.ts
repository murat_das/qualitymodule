export class CreateRuleInput implements ICreateRuleInput {
  name: string;
  min: number;
  max: number;
}

export interface ICreateRuleInput {
  name: string;
  min: number;
  max: number;
}
