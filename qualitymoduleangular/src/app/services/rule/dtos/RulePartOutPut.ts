export class RulePartOutPut implements IRulePartOutPut {
  id: number;
  name: string;
  min: number;
  max: number;
}

export interface IRulePartOutPut {
  id: number;
  name: string;
  min: number;
  max: number;
}
