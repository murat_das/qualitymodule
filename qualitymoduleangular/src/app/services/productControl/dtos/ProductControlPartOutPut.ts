import { ProductPartOutPut } from "@app/services/product/dtos/ProductPartOutPut";

export class ProductControlPartOutPut implements IProductControlPartOutPut{
    id: number;
    amount: number;
    product: ProductPartOutPut;
}

export interface IProductControlPartOutPut{
    id:number;
    amount:number;
    product:ProductPartOutPut;

}
