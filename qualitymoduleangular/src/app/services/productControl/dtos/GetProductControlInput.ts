export class GetProductControlInput implements IGetProductControlInput {
  id: number;
}

export interface IGetProductControlInput {
  id: number;
}
