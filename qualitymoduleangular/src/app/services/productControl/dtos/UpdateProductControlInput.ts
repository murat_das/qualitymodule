import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";

export class UpdateProductControlInput implements IUpdateProductControlInput {
  id: number;
  product: ProductFullOutPut;
  workOrderId: number;
  amount: number;
  seeded: boolean;
}

export interface IUpdateProductControlInput {
  id: number;
  product: ProductFullOutPut;
  workOrderId: number;
  amount: number;
  seeded: boolean;
}
