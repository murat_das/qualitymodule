import { isObject } from "util";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";

export class ProductControlFullOutPut implements IProductControlFullOutPut {
  id: number;
  product: ProductFullOutPut;
  workOrderId: number;
  amount: number;
  seeded: boolean;

  constructor(data?: IProductControlFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.product = data.product;
        this.workOrderId = data.workOrderId;
        this.amount = data.amount;
        this.seeded = data.seeded;
      }
    }
  }

  static fromJS(data: any): ProductControlFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ProductControlFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["product"] = this.product;
    data["workOrderId"] = this.workOrderId;
    data["amount"] = this.amount;
    data["seeded"] = this.seeded;
    return data;
  }

  clone(): ProductControlFullOutPut {
    const json = this.toJSON();
    let result = new ProductControlFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IProductControlFullOutPut {
  id: number;
  product: ProductFullOutPut;
  workOrderId: number;
  amount: number;
  seeded: boolean;
}
