import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";

export class CreateProductControlInput implements ICreateProductControlInput {
  product: ProductFullOutPut;
  workOrderId: number;
  amount: number;
  seeded: boolean=false;
}

export interface ICreateProductControlInput {
  product: ProductFullOutPut;
  workOrderId: number;
  amount: number;
  seeded: boolean;
}
