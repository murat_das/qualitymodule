import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";

export class DeleteProductControlInput implements IDeleteProductControlInput {
  amount: number;
  product: ProductFullOutPut;
  id: number;
}

export interface IDeleteProductControlInput {
  id: number;
  product: ProductFullOutPut;
  amount:number;
}
