/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductControlService } from './productControl.service';

describe('Service: ProductControl', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductControlService]
    });
  });

  it('should ...', inject([ProductControlService], (service: ProductControlService) => {
    expect(service).toBeTruthy();
  }));
});
