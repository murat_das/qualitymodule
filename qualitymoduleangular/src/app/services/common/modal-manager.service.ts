import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { CreateProductDialogComponent } from "@app/components/product/create-product/create-product-dialog.component";
import { EditProductDialogComponent } from "@app/components/product/edit-product/edit-product-dialog.component";
import { GetProductInput } from "../product/dtos/GetProductInput";
import { CreateScrapTypeDialogComponent } from "@app/components/scrapType/create-scrapType/create-scrapType-dialog.component";
import { EditScrapTypeDialogComponent } from "@app/components/scrapType/edit-scrapType/edit-scrapType-dialog.component";
import { CreateTestDialogComponent } from "@app/components/test/create-test/create-test-dialog.component";
import { EditTestDialogComponent } from "@app/components/test/edit-test/edit-test-dialog.component";
import { GetTestInput } from "../test/dtos/GetTestInput";
import { CreateUnitDialogComponent } from "@app/components/unit/create-unit/create-unit-dialog.component";
import { GetUnitInput } from "../unit/dtos/GetUnitInput";
import { EditUnitDialogComponent } from "@app/components/unit/edit-unit/edit-unit-dialog.component";
import { CreateValueTypeDialogComponent } from "@app/components/valueType/create-valueType/create-valueType-dialog.component";
import { GetValueTypeInput } from "../valueType/dtos/GetValueTypeInput";
import { EditValueTypeDialogComponent } from "@app/components/valueType/edit-valueType/edit-valueType-dialog.component";
import { CreateRuleDialogComponent } from "@app/components/rule/create-rule/create-rule-dialog.component";
import { GetRuleInput } from "../rule/dtos/GetRuleInput";
import { EditRuleDialogComponent } from "@app/components/rule/edit-rule/edit-rule-dialog.component";
import { CreateProductTestDialogComponent } from "@app/components/productTest/create-productTest/create-productTest-dialog.component";
import { GetProductTestInput } from "../productTest/dtos/GetProductTestInput";
import { EditProductTestDialogComponent } from "@app/components/productTest/edit-productTest/edit-productTest-dialog.component";
import { CreateProductScrapDialogComponent } from "@app/components/productScrap/create-productScrap/create-productScrap-dialog.component";
import { GetProductScrapInput } from "../productScrap/dtos/GetProductScrapInput";
import { EditProductScrapDialogComponent } from "@app/components/productScrap/edit-productScrap/edit-productScrap-dialog.component";
import { CreateProductControlDialogComponent } from "@app/components/productControl/create-productControl/create-productControl-dialog.component";
import { GetProductControlInput } from "../productControl/dtos/GetProductControlInput";
import { EditProductControlDialogComponent } from "@app/components/productControl/edit-productControl/edit-productControl-dialog.component";
import { CreateScrapDialogComponent } from "@app/components/scrap/create-scrap/create-scrap-dialog.component";
import { GetScrapInput } from "../scrap/dtos/GetScrapInput";
import { EditScrapDialogComponent } from "@app/components/scrap/edit-scrap/edit-scrap-dialog.component";

@Injectable({
  providedIn: "root",
})
export class ModalManagerService {
  constructor(private _dialog: MatDialog) {}

  // Products
  openCreateProductDialog(parameters?: any): any {
    let createProductDialog;
    createProductDialog = this._dialog.open(CreateProductDialogComponent, {
      data: parameters,
    });

    return createProductDialog;
  }

  openEditProductDialog(id?: number): any {
    let editProductDialog;
    let getProductInput = new GetProductInput();
    getProductInput.id = id;

    editProductDialog = this._dialog.open(EditProductDialogComponent, {
      data: getProductInput,
    });

    return editProductDialog;
  }

  // ScrapTypes
  openCreateScrapTypeDialog(parameters?: any): any {
    let createScrapTypeDialog;
    createScrapTypeDialog = this._dialog.open(CreateScrapTypeDialogComponent, {
      data: parameters,
    });

    return createScrapTypeDialog;
  }

  openEditScrapTypeDialog(id?: number): any {
    let editScrapTypeDialog;
    let getScrapTypeInput = new GetProductInput();
    getScrapTypeInput.id = id;

    editScrapTypeDialog = this._dialog.open(EditScrapTypeDialogComponent, {
      data: getScrapTypeInput,
    });

    return editScrapTypeDialog;
  }

  // Tests
  openCreateTestDialog(parameters?: any): any {
    let createTestDialog;
    createTestDialog = this._dialog.open(CreateTestDialogComponent, {
      data: parameters,
    });

    return createTestDialog;
  }

  openEditTestDialog(id?: number): any {
    let editTestDialog;
    let getTestInput = new GetTestInput();
    getTestInput.id = id;

    editTestDialog = this._dialog.open(EditTestDialogComponent, {
      data: getTestInput,
    });

    return editTestDialog;
  }

  // Units
  openCreateUnitDialog(parameters?: any): any {
    let createUnitDialog;
    createUnitDialog = this._dialog.open(CreateUnitDialogComponent, {
      data: parameters,
    });

    return createUnitDialog;
  }

  openEditUnitDialog(id?: number): any {
    let editUnitDialog;
    let getUnitInput = new GetUnitInput();
    getUnitInput.id = id;

    editUnitDialog = this._dialog.open(EditUnitDialogComponent, {
      data: getUnitInput,
    });

    return editUnitDialog;
  }

  // ValueTypes
  openCreateValueTypeDialog(parameters?: any): any {
    let createValueTypeDialog;
    createValueTypeDialog = this._dialog.open(CreateValueTypeDialogComponent, {
      data: parameters,
    });

    return createValueTypeDialog;
  }

  openEditValueTypeDialog(id?: number): any {
    let editValueTypeDialog;
    let getValueTypeInput = new GetValueTypeInput();
    getValueTypeInput.id = id;

    editValueTypeDialog = this._dialog.open(EditValueTypeDialogComponent, {
      data: getValueTypeInput,
    });

    return editValueTypeDialog;
  }

  // Rules
  openCreateRuleDialog(parameters?: any): any {
    let createRuleDialog;
    createRuleDialog = this._dialog.open(CreateRuleDialogComponent, {
      data: parameters,
    });

    return createRuleDialog;
  }

  openEditRuleDialog(id?: number): any {
    let editRuleDialog;
    let getRuleInput = new GetRuleInput();
    getRuleInput.id = id;

    editRuleDialog = this._dialog.open(EditRuleDialogComponent, {
      data: getRuleInput,
    });

    return editRuleDialog;
  }

  // ProductTests
  openCreateProductTestDialog(parameters?: any): any {
    let createProductTestDialog;
    createProductTestDialog = this._dialog.open(
      CreateProductTestDialogComponent,
      {
        data: parameters,
      }
    );

    return createProductTestDialog;
  }

  openEditProductTestDialog(id?: number): any {
    let editProductTestDialog;
    let getProductTestInput = new GetProductTestInput();
    getProductTestInput.id = id;

    editProductTestDialog = this._dialog.open(EditProductTestDialogComponent, {
      data: getProductTestInput,
    });

    return editProductTestDialog;
  }

  // ProductScraps
  openCreateProductScrapDialog(parameters?: any): any {
    let createProductScrapDialog;
    createProductScrapDialog = this._dialog.open(
      CreateProductScrapDialogComponent,
      {
        data: parameters,
      }
    );

    return createProductScrapDialog;
  }

  openEditProductScrapDialog(id?: number): any {
    let editProductScrapDialog;
    let getProductScrapInput = new GetProductScrapInput();
    getProductScrapInput.id = id;

    editProductScrapDialog = this._dialog.open(
      EditProductScrapDialogComponent,
      {
        data: getProductScrapInput,
      }
    );

    return editProductScrapDialog;
  }

  // ProductControls
  openCreateProductControlDialog(parameters?: any): any {
    let createProductControlDialog;
    createProductControlDialog = this._dialog.open(
      CreateProductControlDialogComponent,
      {
        data: parameters,
      }
    );

    return createProductControlDialog;
  }

  openEditProductControlDialog(id?: number): any {
    let editProductControlDialog;
    let getProductControlInput = new GetProductControlInput();
    getProductControlInput.id = id;

    editProductControlDialog = this._dialog.open(
      EditProductControlDialogComponent,
      {
        data: getProductControlInput,
      }
    );

    return editProductControlDialog;
  }

  // Scraps
  openCreateScrapDialog(parameters?: any): any {
    let createScrapDialog;
    console.log(parameters);

    createScrapDialog = this._dialog.open(CreateScrapDialogComponent, {
      width: "30%",
      maxWidth: "100vw",
      data: parameters,
    });

    return createScrapDialog;
  }

  openEditScrapDialog(id?: number): any {
    let editScrapDialog;
    let getScrapInput = new GetScrapInput();
    getScrapInput.id = id;

    editScrapDialog = this._dialog.open(EditScrapDialogComponent, {
      data: getScrapInput,
    });

    return editScrapDialog;
  }
}
