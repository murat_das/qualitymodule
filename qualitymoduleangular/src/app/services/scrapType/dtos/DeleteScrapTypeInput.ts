export class DeleteScrapTypeInput implements ICreateScrapTypeInput {
    id: number;
    name: string;
}

export interface ICreateScrapTypeInput{
    id:number;
    name:string;
}
