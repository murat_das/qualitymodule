import { isObject } from "util";

export class ScrapTypeFullOutPut implements IScrapTypeFullOutPut {
  name: string;
  id: number;

  constructor(data?: IScrapTypeFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.name = data.name;
      }
    }
  }

  static fromJS(data: any): ScrapTypeFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ScrapTypeFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["name"] = this.name;
    return data;
  }

  clone(): ScrapTypeFullOutPut {
    const json = this.toJSON();
    let result = new ScrapTypeFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IScrapTypeFullOutPut {
  id: number;
  name: string;
}
