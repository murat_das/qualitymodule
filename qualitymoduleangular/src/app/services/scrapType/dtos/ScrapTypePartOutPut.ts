export class ScrapTypePartOutPut implements IScrapTypePartOutPut {
  id: number;
  name: string;
}

export interface IScrapTypePartOutPut {
  id: number;
  name: string;
}
