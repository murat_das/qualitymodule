export class CreateScrapTypeInput implements ICreateScrapTypeInput {
    id: number;
    name: string;
}

export interface ICreateScrapTypeInput{
    id:number;
    name:string;
}
