export class UpdateScrapTypeInput implements IUpdateScrapTypeInput {
  id: number;
  name: string;
}

export interface IUpdateScrapTypeInput {
  id: number;
  name: string;
}
