export class UpdateProductInput implements IUpdateProductInput {
    name:string;
    id: number;
}

export interface IUpdateProductInput {
    name:string;
    id:number;
}
