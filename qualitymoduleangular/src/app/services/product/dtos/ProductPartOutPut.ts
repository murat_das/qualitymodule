export class ProductPartOutPut implements IProductPartOutPut {
    id: number;
    name: string;
}


export interface IProductPartOutPut{
    id:number;
    name:string;
}