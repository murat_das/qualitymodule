export class CreateProductInput implements ICreateProductInput {
    id: number;
    name: string;
}

export interface ICreateProductInput{
    id:number;
    name:string;
}
