export class DeleteProductInput implements IDeleteProductInput {
    name: string;
    id: number;
}

export interface IDeleteProductInput{
    name: string;
    id:number;
}
