export class GetProductInput implements IGetProductInput {
    id: number;
}

export interface IGetProductInput{
    id:number;
}
