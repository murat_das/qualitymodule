import { NgModule } from '@angular/core';
import { UtilsService } from './common/utils.service';
import { ModalManagerService } from './common/modal-manager.service';
import { ProductComponent } from '@app/components/product/product.component';
import { ScrapTypeComponent } from '@app/components/scrapType/scrapType.component';
import { TestComponent } from '@app/components/test/test.component';
import { UnitComponent } from '@app/components/unit/unit.component';
import { ValueTypeComponent } from '@app/components/valueType/valueType.component';
import { RuleComponent } from '@app/components/rule/rule.component';
import { ProductTestComponent } from '@app/components/productTest/productTest.component';
import { ProductScrapComponent } from '@app/components/productScrap/productScrap.component';
import { ScrapComponent } from '@app/components/scrap/scrap.component';
import { ProductControlComponent } from '@app/components/productControl/productControl.component';

@NgModule({
    providers: [
      UtilsService,
      ModalManagerService,
      ProductComponent,
      ScrapTypeComponent,
      TestComponent,
      UnitComponent,
      ValueTypeComponent,
      RuleComponent,
      ProductTestComponent,
      ProductScrapComponent,
      ScrapComponent,
      ProductControlComponent,
    ]
  })

export class QualityModuleServiceModule {}
