import { isObject } from "util";

export class TestFullOutPut implements ITestFullOutPut {
  id: number;
  name: string;

  constructor(data?: ITestFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.name = data.name;
      }
    }
  }

  static fromJS(data: any): TestFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new TestFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["name"] = this.name;
    return data;
  }

  clone(): TestFullOutPut {
    const json = this.toJSON();
    let result = new TestFullOutPut();
    result.init(json);
    return result;
  }
}

export interface ITestFullOutPut {
  id: number;
  name: string;
}
