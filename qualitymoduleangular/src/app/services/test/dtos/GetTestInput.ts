export class GetTestInput implements IGetTestInput {
  id: number;
}

export interface IGetTestInput {
  id: number;
}
