export class DeleteTestInput implements IDeleteTestInput {
  id: number;
  name:string;
}

export interface IDeleteTestInput {
  id: number;
  name:string;

}
