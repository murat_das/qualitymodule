export class UpdateTestInput implements IUpdateTestInput {
  id: number;
  name: string;
}

export interface IUpdateTestInput {
  id: number;
  name: string;
}
