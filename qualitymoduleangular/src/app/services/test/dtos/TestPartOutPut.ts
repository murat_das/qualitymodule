export class TestPartOutPut implements ITestPartOutPut {
  id: number;
  name: string;
}

export interface ITestPartOutPut {
  id: number;
  name: string;
}
