export class CreateTestInput implements ICreateTestInput {
  id: number;
  name: string;
}

export interface ICreateTestInput {
  id: number;
  name: string;
}
