import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";
import { TestService } from "@app/services/test/test.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteTestInput } from "@app/services/test/dtos/DeleteTestInput";

@Component({
  selector: "app-test",
  templateUrl: "./test.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./test.component.css"],
  providers: []
})
export class TestComponent extends AppComponentBase implements OnInit {
  tests: TestFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _testService: TestService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient : HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource=this.createDxDataSource();
  }

  createTest(): void {
    this._modalManagerService.openCreateTestDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editTest(id: number): void {
    this._modalManagerService.openEditTestDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteTest(test: DeleteTestInput): void {
    abp.message.confirm(
      this.l('TestDeleteWarningMessage', test.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._testService
            .delete(test)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      );
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._testService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: TestFullOutPut[]) => {
                this.tests = result;
                resolve(this.tests);
              });
          });
        }
      })
    });
  }
}
