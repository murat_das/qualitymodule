import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { TestFullOutPut } from '@app/services/test/dtos/TestFullOutPut';
import { TestService } from '@app/services/test/test.service';
import { GetTestInput } from '@app/services/test/dtos/GetTestInput';

@Component({
  selector: 'app-edit-test-dialog',
  templateUrl: './edit-test-dialog.component.html',
  styleUrls: ['./edit-test-dialog.component.css']
})
export class EditTestDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  test: TestFullOutPut = new TestFullOutPut();

  constructor(injector: Injector,
    public _testService: TestService,
    private _dialogRef: MatDialogRef<EditTestDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _test: GetTestInput) {
    super(injector);
  }

  ngOnInit() {
    this._testService.get(this._test).subscribe((result: TestFullOutPut) => {
      this.test = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._testService
      .update({name:this.test.name, id: this.test.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
