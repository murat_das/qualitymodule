import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateTestInput } from '@app/services/test/dtos/CreateTestInput';
import { TestService } from '@app/services/test/test.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-test-dialog',
  templateUrl: './create-test-dialog.component.html',
  styleUrls: ['./create-test-dialog.component.css']
})
export class CreateTestDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  test: CreateTestInput = new CreateTestInput();

  constructor(injector: Injector,
    public _testService: TestService,
    private _dialogRef: MatDialogRef<CreateTestDialogComponent>) {
    super(injector);
  }

  ngOnInit() {

  }

  save(): void {
    this.saving = true;

    this._testService
      .create(this.test)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
