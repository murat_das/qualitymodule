import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ScrapTypeService } from "@app/services/scrapType/scrapType.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";
import { DeleteScrapTypeInput } from "@app/services/scrapType/dtos/DeleteScrapTypeInput";

@Component({
  selector: "app-scrapType",
  templateUrl: "./scrapType.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./scrapType.component.css"],
  providers: [],
})
export class ScrapTypeComponent extends AppComponentBase implements OnInit {
  scrapTypes: ScrapTypeFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _scrapTypeService: ScrapTypeService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createScrapType(): void {
    this._modalManagerService
      .openCreateScrapTypeDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editScrapType(id: number): void {
    this._modalManagerService
      .openEditScrapTypeDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteScrapType(scrapType: DeleteScrapTypeInput): void {
    abp.message.confirm(
      this.l("ScrapTypeDeleteWarningMessage", scrapType.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._scrapTypeService
            .delete(scrapType)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      // {
      //   location: "before",
      //   template: "formNameTemplate",
      // },
      // {
      //   location: "after",
      //   template: "refreshButtonTemplate",
      // }
    );
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._scrapTypeService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ScrapTypeFullOutPut[]) => {
                this.scrapTypes = result;
                resolve(this.scrapTypes);
              });
          });
        },
      }),
    });
  }
}
