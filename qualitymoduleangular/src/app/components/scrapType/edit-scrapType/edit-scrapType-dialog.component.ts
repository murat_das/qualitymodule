import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { ScrapTypeFullOutPut } from '@app/services/scrapType/dtos/ScrapTypeFullOutPut';
import { ScrapTypeService } from '@app/services/scrapType/scrapType.service';
import { GetScrapTypeInput } from '@app/services/scrapType/dtos/GetScrapTypeInput';

@Component({
  selector: 'app-edit-scrapType-dialog',
  templateUrl: './edit-scrapType-dialog.component.html',
  styleUrls: ['./edit-scrapType-dialog.component.css']
})
export class EditScrapTypeDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  scrapType: ScrapTypeFullOutPut = new ScrapTypeFullOutPut();

  constructor(injector: Injector,
    public _scrapTypeService: ScrapTypeService,
    private _dialogRef: MatDialogRef<EditScrapTypeDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _scrapType: GetScrapTypeInput) {
    super(injector);
  }

  ngOnInit() {
    this._scrapTypeService.get(this._scrapType).subscribe((result: ScrapTypeFullOutPut) => {
      this.scrapType = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._scrapTypeService
      .update({name:this.scrapType.name, id: this.scrapType.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
