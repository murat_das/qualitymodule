import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateScrapTypeInput } from '@app/services/scrapType/dtos/CreateScrapTypeInput';
import { ScrapTypeService } from '@app/services/scrapType/scrapType.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-scrapType-dialog',
  templateUrl: './create-scrapType-dialog.component.html',
  styleUrls: ['./create-scrapType-dialog.component.css']
})
export class CreateScrapTypeDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  scrapType: CreateScrapTypeInput = new CreateScrapTypeInput();

  constructor(injector: Injector,
    public _scrapTypeService: ScrapTypeService,
    private _dialogRef: MatDialogRef<CreateScrapTypeDialogComponent>) {
    super(injector);
  }

  ngOnInit() {

  }

  save(): void {
    this.saving = true;

    this._scrapTypeService
      .create(this.scrapType)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
