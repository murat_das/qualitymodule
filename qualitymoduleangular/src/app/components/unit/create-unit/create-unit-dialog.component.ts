import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateUnitInput } from '@app/services/unit/dtos/CreateUnitInput';
import { UnitService } from '@app/services/unit/unit.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-unit-dialog',
  templateUrl: './create-unit-dialog.component.html',
  styleUrls: ['./create-unit-dialog.component.css']
})
export class CreateUnitDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  unit: CreateUnitInput = new CreateUnitInput();

  constructor(injector: Injector,
    public _unitService: UnitService,
    private _dialogRef: MatDialogRef<CreateUnitDialogComponent>) {
    super(injector);
  }

  ngOnInit() {

  }

  save(): void {
    this.saving = true;

    this._unitService
      .create(this.unit)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
