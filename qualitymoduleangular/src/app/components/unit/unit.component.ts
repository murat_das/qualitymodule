import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { UnitFullOutPut } from "@app/services/unit/dtos/UnitFullOutPut";
import { UnitService } from "@app/services/unit/unit.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteUnitInput } from "@app/services/unit/dtos/DeleteUnitInput";

@Component({
  selector: "app-unit",
  templateUrl: "./unit.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./unit.component.css"],
  providers: []
})
export class UnitComponent extends AppComponentBase implements OnInit {
  units: UnitFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _unitService: UnitService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient : HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource=this.createDxDataSource();
  }

  createUnit(): void {
    this._modalManagerService.openCreateUnitDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editUnit(id: number): void {
    this._modalManagerService.openEditUnitDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteUnit(unit: DeleteUnitInput): void {
    abp.message.confirm(
      this.l('UnitDeleteWarningMessage', unit.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._unitService
            .delete(unit)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      );
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._unitService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: UnitFullOutPut[]) => {
                this.units = result;
                resolve(this.units);
              });
          });
        }
      })
    });
  }
}
