import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { UnitFullOutPut } from '@app/services/unit/dtos/UnitFullOutPut';
import { UnitService } from '@app/services/unit/unit.service';
import { GetUnitInput } from '@app/services/unit/dtos/GetUnitInput';

@Component({
  selector: 'app-edit-unit-dialog',
  templateUrl: './edit-unit-dialog.component.html',
  styleUrls: ['./edit-unit-dialog.component.css']
})
export class EditUnitDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  unit: UnitFullOutPut = new UnitFullOutPut();

  constructor(injector: Injector,
    public _unitService: UnitService,
    private _dialogRef: MatDialogRef<EditUnitDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _unit: GetUnitInput) {
    super(injector);
  }

  ngOnInit() {
    this._unitService.get(this._unit).subscribe((result: UnitFullOutPut) => {
      this.unit = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._unitService
      .update({name:this.unit.name, id: this.unit.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
