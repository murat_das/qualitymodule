import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateValueTypeInput } from '@app/services/valueType/dtos/CreateValueTypeInput';
import { ValueTypeService } from '@app/services/valueType/valueType.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-valueType-dialog',
  templateUrl: './create-valueType-dialog.component.html',
  styleUrls: ['./create-valueType-dialog.component.css']
})
export class CreateValueTypeDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  valueType: CreateValueTypeInput = new CreateValueTypeInput();

  constructor(injector: Injector,
    public _valueTypeService: ValueTypeService,
    private _dialogRef: MatDialogRef<CreateValueTypeDialogComponent>) {
    super(injector);
  }

  ngOnInit() {

  }

  save(): void {
    this.saving = true;

    this._valueTypeService
      .create(this.valueType)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
