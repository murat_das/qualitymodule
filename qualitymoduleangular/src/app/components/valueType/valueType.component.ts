import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ValueTypeFullOutPut } from "@app/services/valueType/dtos/ValueTypeFullOutPut";
import { ValueTypeService } from "@app/services/valueType/valueType.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteValueTypeInput } from "@app/services/valueType/dtos/DeleteValueTypeInput";

@Component({
  selector: "app-valueType",
  templateUrl: "./valueType.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./valueType.component.css"],
  providers: [],
})
export class ValueTypeComponent extends AppComponentBase implements OnInit {
  valueTypes: ValueTypeFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _valueTypeService: ValueTypeService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createValueType(): void {
    this._modalManagerService
      .openCreateValueTypeDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editValueType(id: number): void {
    this._modalManagerService
      .openEditValueTypeDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteValueType(valueType: DeleteValueTypeInput): void {
    abp.message.confirm(
      this.l("ValueTypeDeleteWarningMessage", valueType.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._valueTypeService
            .delete(valueType)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._valueTypeService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ValueTypeFullOutPut[]) => {
                this.valueTypes = result;
                resolve(this.valueTypes);
              });
          });
        },
      }),
    });
  }
}
