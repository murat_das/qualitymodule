import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { ValueTypeFullOutPut } from '@app/services/valueType/dtos/ValueTypeFullOutPut';
import { ValueTypeService } from '@app/services/valueType/valueType.service';
import { GetValueTypeInput } from '@app/services/valueType/dtos/GetValueTypeInput';

@Component({
  selector: 'app-edit-valueType-dialog',
  templateUrl: './edit-valueType-dialog.component.html',
  styleUrls: ['./edit-valueType-dialog.component.css']
})
export class EditValueTypeDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  valueType: ValueTypeFullOutPut = new ValueTypeFullOutPut();

  constructor(injector: Injector,
    public _valueTypeService: ValueTypeService,
    private _dialogRef: MatDialogRef<EditValueTypeDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _valueType: GetValueTypeInput) {
    super(injector);
  }

  ngOnInit() {
    this._valueTypeService.get(this._valueType).subscribe((result: ValueTypeFullOutPut) => {
      this.valueType = result;
    });
  }

  save(): void {
    this.saving = true;
    this._valueTypeService
      .update({name:this.valueType.name, id: this.valueType.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
