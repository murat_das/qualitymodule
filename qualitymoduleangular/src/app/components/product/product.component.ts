import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ProductService } from "@app/services/product/product.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteProductInput } from "@app/services/product/dtos/DeleteProductInput";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./product.component.css"],
  providers: []
})
export class ProductComponent extends AppComponentBase implements OnInit {
  products: ProductFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _productService: ProductService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient : HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource=this.createDxDataSource();
  }

  createProduct(): void {
    this._modalManagerService.openCreateProductDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editProduct(id: number): void {
    this._modalManagerService.openEditProductDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteProduct(product: DeleteProductInput): void {
    abp.message.confirm(
      this.l('ProductDeleteWarningMessage', product.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._productService
            .delete(product)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      );
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._productService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ProductFullOutPut[]) => {
                this.products = result;
                resolve(this.products);
              });
          });
        }
      })
    });
  }
}
