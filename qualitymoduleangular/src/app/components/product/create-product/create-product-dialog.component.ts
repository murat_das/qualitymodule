import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateProductInput } from '@app/services/product/dtos/CreateProductInput';
import { ProductService } from '@app/services/product/product.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-product-dialog',
  templateUrl: './create-product-dialog.component.html',
  styleUrls: ['./create-product-dialog.component.css']
})
export class CreateProductDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  product: CreateProductInput = new CreateProductInput();

  constructor(injector: Injector,
    public _productService: ProductService,
    private _dialogRef: MatDialogRef<CreateProductDialogComponent>) {
    super(injector);
  }

  ngOnInit() {

  }

  save(): void {
    this.saving = true;

    this._productService
      .create(this.product)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
