import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { ProductFullOutPut } from '@app/services/product/dtos/ProductFullOutPut';
import { ProductService } from '@app/services/product/product.service';
import { GetProductInput } from '@app/services/product/dtos/GetProductInput';

@Component({
  selector: 'app-edit-product-dialog',
  templateUrl: './edit-product-dialog.component.html',
  styleUrls: ['./edit-product-dialog.component.css']
})
export class EditProductDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  product: ProductFullOutPut = new ProductFullOutPut();

  constructor(injector: Injector,
    public _productService: ProductService,
    private _dialogRef: MatDialogRef<EditProductDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _product: GetProductInput) {
    super(injector);
  }

  ngOnInit() {
    this._productService.get(this._product).subscribe((result: ProductFullOutPut) => {
      this.product = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._productService
      .update({name:this.product.name, id: this.product.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
