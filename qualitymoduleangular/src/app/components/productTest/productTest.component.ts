import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ProductTestFullOutPut } from "@app/services/productTest/dtos/ProductTestFullOutPut";
import { ProductTestService } from "@app/services/productTest/productTest.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteProductTestInput } from "@app/services/productTest/dtos/DeleteProductTestInput";

@Component({
  selector: "app-productTest",
  templateUrl: "./productTest.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./productTest.component.css"],
  providers: [],
})
export class ProductTestComponent extends AppComponentBase implements OnInit {
  productTests: ProductTestFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _productTestService: ProductTestService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createProductTest(): void {
    this._modalManagerService
      .openCreateProductTestDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editProductTest(id: number): void {
    this._modalManagerService
      .openEditProductTestDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteProductTest(productTest: DeleteProductTestInput): void {
    abp.message.confirm(
      this.l(
        "ProductTestDeleteWarningMessage",
        productTest.product.name + "-" + productTest.test.name
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._productTestService
            .delete(productTest)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._productTestService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ProductTestFullOutPut[]) => {
                this.productTests = result;
                resolve(this.productTests);
              });
          });
        },
      }),
    });
  }
}
