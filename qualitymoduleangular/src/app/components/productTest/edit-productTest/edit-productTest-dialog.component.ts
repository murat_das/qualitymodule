import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { ProductTestFullOutPut } from '@app/services/productTest/dtos/ProductTestFullOutPut';
import { ProductTestService } from '@app/services/productTest/productTest.service';
import { GetProductTestInput } from '@app/services/productTest/dtos/GetProductTestInput';
import { ProductFullOutPut } from '@app/services/product/dtos/ProductFullOutPut';
import { TestFullOutPut } from '@app/services/test/dtos/TestFullOutPut';
import { ProductService } from '@app/services/product/product.service';
import { TestService } from '@app/services/test/test.service';

@Component({
  selector: 'app-edit-productTest-dialog',
  templateUrl: './edit-productTest-dialog.component.html',
  styleUrls: ['./edit-productTest-dialog.component.css']
})
export class EditProductTestDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  productTest: ProductTestFullOutPut = new ProductTestFullOutPut();
  products: ProductFullOutPut[] = [];
  tests: TestFullOutPut[] = [];

  constructor(injector: Injector,
    public _productTestService: ProductTestService,
    public _productService: ProductService,
    public _testService: TestService,
    private _dialogRef: MatDialogRef<EditProductTestDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _productTest: GetProductTestInput) {
    super(injector);
  }

  ngOnInit() {
    this._productTestService.get(this._productTest).subscribe((result: ProductTestFullOutPut) => {
      this.productTest = result;
    });

    this.getProducts();
    this.getTests();
  }

  getProducts(): ProductFullOutPut[] | any {
    this._productService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductFullOutPut[]) => {
        return (this.products = result);
      });
  }

  getTests(): TestFullOutPut[] | any {
    this._testService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: TestFullOutPut[]) => {
        return (this.tests = result);
      });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._productTestService
      .update({id: this.productTest.id,product:this.productTest.product,test:this.productTest.test})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  customCompare(o1, o2) {
    return o1.id === o2.id;
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
