import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateProductTestInput } from "@app/services/productTest/dtos/CreateProductTestInput";
import { ProductTestService } from "@app/services/productTest/productTest.service";
import { MatDialogRef } from "@angular/material";
import { finalize } from "rxjs/operators";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ProductService } from "@app/services/product/product.service";
import { TestService } from "@app/services/test/test.service";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";

@Component({
  selector: "app-create-productTest-dialog",
  templateUrl: "./create-productTest-dialog.component.html",
  styleUrls: ["./create-productTest-dialog.component.css"],
})
export class CreateProductTestDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  productTest: CreateProductTestInput = new CreateProductTestInput();
  products: ProductFullOutPut[] = [];
  tests: TestFullOutPut[] = [];

  constructor(
    injector: Injector,
    public _productTestService: ProductTestService,
    public _productService: ProductService,
    public _testService: TestService,
    private _dialogRef: MatDialogRef<CreateProductTestDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getProducts();
    this.getTests();
  }

  getProducts(): ProductFullOutPut[] | any {
    this._productService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductFullOutPut[]) => {
        return (this.products = result);
      });
  }

  getTests(): TestFullOutPut[] | any {
    this._testService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: TestFullOutPut[]) => {
        return (this.tests = result);
      });
  }

  save(): void {
    this.saving = true;

    this._productTestService
      .create(this.productTest)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
