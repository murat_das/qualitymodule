import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateScrapInput } from "@app/services/scrap/dtos/CreateScrapInput";
import { ScrapService } from "@app/services/scrap/scrap.service";
import { MatDialogRef, MatDialogConfig } from "@angular/material";
import { finalize } from "rxjs/operators";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { TestFullOutPut } from "@app/services/test/dtos/TestFullOutPut";
import { ProductScrapFullOutPut } from "@app/services/productScrap/dtos/ProductScrapFullOutPut";
import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";
import { ProductScrapService } from "@app/services/productScrap/productScrap.service";
import { ProductControlService } from "@app/services/productControl/productControl.service";
import DataSource from "devextreme/data/data_source";
import CustomStore from "devextreme/data/custom_store";

@Component({
  selector: "app-create-scrap-dialog",
  templateUrl: "./create-scrap-dialog.component.html",
  styleUrls: ["./create-scrap-dialog.component.css"],
})
export class CreateScrapDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  scrap: CreateScrapInput = new CreateScrapInput();
  productScraps: ProductScrapFullOutPut[] = [];
  productControls: ProductControlFullOutPut[] = [];
  gridBoxValue: number[] = [2];
  dataSource:any;
  constructor(
    injector: Injector,
    public _scrapService: ScrapService,
    public _productScrapService: ProductScrapService,
    public _productControlService: ProductControlService,
    private _dialogRef: MatDialogRef<CreateScrapDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getProductScraps();
    this.getProductControls();
    this.dataSource=this.createDxDataSource();
  }

  gridBox_displayExpr(item){
    console.log(item);
    
    return item && item.product.name + " & " + item.scrapType.name;
}

createDxDataSource(): DataSource {
  return new DataSource({
    store: new CustomStore({
      key: "id",
      loadMode: "raw",
      load: () => {
        return new Promise((resolve, reject) => {
          this._productScrapService
            .getList()
            .pipe(
              finalize(() => {
                return null;
              })
            )
            .subscribe((result: ProductScrapFullOutPut[]) => {
              this.productScraps = result;
              console.log(this.productScraps,"this.productScraps");
              
              resolve(this.productScraps);
            });
        });
      },
    }),
  });
}

  getProductScraps(): ProductScrapFullOutPut[] | any {
    this._productScrapService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductScrapFullOutPut[]) => {
        return (this.productScraps = result);
      });
  }

  getProductControls(): ProductControlFullOutPut[] | any {
    this._productControlService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductControlFullOutPut[]) => {
        return (this.productControls = result);
      });
  }

  save(): void {
    this.saving = true;

    this._scrapService
      .create(this.scrap)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
