import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ScrapFullOutPut } from "@app/services/scrap/dtos/ScrapFullOutPut";
import { ScrapService } from "@app/services/scrap/scrap.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteScrapInput } from "@app/services/scrap/dtos/DeleteScrapInput";

@Component({
  selector: "app-scrap",
  templateUrl: "./scrap.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./scrap.component.css"],
  providers: [],
})
export class ScrapComponent extends AppComponentBase implements OnInit {
  scraps: ScrapFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _scrapService: ScrapService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createScrap(): void {
  
    this._modalManagerService
      .openCreateScrapDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editScrap(id: number): void {
    this._modalManagerService
      .openEditScrapDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteScrap(scrap: DeleteScrapInput): void {
    abp.message.confirm(
      this.l("ScrapDeleteWarningMessage", scrap.id),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._scrapService
            .delete(scrap)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
    console.log(this.dataSource, "this.dataSource");
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._scrapService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ScrapFullOutPut[]) => {
                this.scraps = result;
                resolve(this.scraps);
              });
          });
        },
      }),
    });
  }
}
