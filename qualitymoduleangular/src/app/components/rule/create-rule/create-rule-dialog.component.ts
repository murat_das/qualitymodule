import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateRuleInput } from '@app/services/rule/dtos/CreateRuleInput';
import { RuleService } from '@app/services/rule/rule.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-rule-dialog',
  templateUrl: './create-rule-dialog.component.html',
  styleUrls: ['./create-rule-dialog.component.css']
})
export class CreateRuleDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  rule: CreateRuleInput = new CreateRuleInput();

  constructor(injector: Injector,
    public _ruleService: RuleService,
    private _dialogRef: MatDialogRef<CreateRuleDialogComponent>) {
    super(injector);
  }

  ngOnInit() {

  }

  save(): void {
    this.saving = true;

    this._ruleService
      .create(this.rule)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
