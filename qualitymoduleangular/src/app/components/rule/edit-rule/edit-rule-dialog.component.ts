import { Component, OnInit, Injector, Optional, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/app-component-base";
import { RuleFullOutPut } from "@app/services/rule/dtos/RuleFullOutPut";
import { RuleService } from "@app/services/rule/rule.service";
import { GetRuleInput } from "@app/services/rule/dtos/GetRuleInput";

@Component({
  selector: "app-edit-rule-dialog",
  templateUrl: "./edit-rule-dialog.component.html",
  styleUrls: ["./edit-rule-dialog.component.css"],
})
export class EditRuleDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  rule: RuleFullOutPut = new RuleFullOutPut();

  constructor(
    injector: Injector,
    public _ruleService: RuleService,
    private _dialogRef: MatDialogRef<EditRuleDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _rule: GetRuleInput
  ) {
    super(injector);
  }

  ngOnInit() {
    this._ruleService.get(this._rule).subscribe((result: RuleFullOutPut) => {
      this.rule = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._ruleService
      .update({
        name: this.rule.name,
        id: this.rule.id,
        min: this.rule.min,
        max: this.rule.max,
      })
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
