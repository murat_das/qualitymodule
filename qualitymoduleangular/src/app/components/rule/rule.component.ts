import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { RuleFullOutPut } from "@app/services/rule/dtos/RuleFullOutPut";
import { RuleService } from "@app/services/rule/rule.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteRuleInput } from "@app/services/rule/dtos/DeleteRuleInput";

@Component({
  selector: "app-rule",
  templateUrl: "./rule.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./rule.component.css"],
  providers: []
})
export class RuleComponent extends AppComponentBase implements OnInit {
  rules: RuleFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _ruleService: RuleService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient : HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource=this.createDxDataSource();
  }

  createRule(): void {
    this._modalManagerService.openCreateRuleDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editRule(id: number): void {
    this._modalManagerService.openEditRuleDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteRule(rule: DeleteRuleInput): void {
    abp.message.confirm(
      this.l('RuleDeleteWarningMessage', rule.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._ruleService
            .delete(rule)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      );
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._ruleService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: RuleFullOutPut[]) => {
                this.rules = result;
                resolve(this.rules);
              });
          });
        }
      })
    });
  }
}
