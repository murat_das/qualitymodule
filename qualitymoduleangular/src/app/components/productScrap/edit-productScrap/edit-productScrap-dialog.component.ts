import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { ProductScrapFullOutPut } from '@app/services/productScrap/dtos/ProductScrapFullOutPut';
import { ProductScrapService } from '@app/services/productScrap/productScrap.service';
import { GetProductScrapInput } from '@app/services/productScrap/dtos/GetProductScrapInput';
import { ProductFullOutPut } from '@app/services/product/dtos/ProductFullOutPut';
import { ProductService } from '@app/services/product/product.service';
import { ScrapTypeFullOutPut } from '@app/services/scrapType/dtos/ScrapTypeFullOutPut';
import { ScrapTypeService } from '@app/services/scrapType/scrapType.service';

@Component({
  selector: 'app-edit-productScrap-dialog',
  templateUrl: './edit-productScrap-dialog.component.html',
  styleUrls: ['./edit-productScrap-dialog.component.css']
})
export class EditProductScrapDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  productScrap: ProductScrapFullOutPut = new ProductScrapFullOutPut();
  products: ProductFullOutPut[] = [];
  scrapTypes: ScrapTypeFullOutPut[] = [];

  constructor(injector: Injector,
    public _productScrapService: ProductScrapService,
    public _productService: ProductService,
    public _scrapTypeService: ScrapTypeService,
    private _dialogRef: MatDialogRef<EditProductScrapDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _productScrap: GetProductScrapInput) {
    super(injector);
  }

  ngOnInit() {
    this._productScrapService.get(this._productScrap).subscribe((result: ProductScrapFullOutPut) => {
      this.productScrap = result;
    });

    this.getProducts();
    this.getScrapTypes();
  }

  getProducts(): ProductFullOutPut[] | any {
    this._productService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductFullOutPut[]) => {
        return (this.products = result);
      });
  }

  getScrapTypes(): ScrapTypeFullOutPut[] | any {
    this._scrapTypeService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ScrapTypeFullOutPut[]) => {
        return (this.scrapTypes = result);
      });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._productScrapService
      .update({id: this.productScrap.id,product:this.productScrap.product,scrapType:this.productScrap.scrapType})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  customCompare(o1, o2) {
    return o1.id === o2.id;
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
