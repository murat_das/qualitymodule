import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateProductScrapInput } from "@app/services/productScrap/dtos/CreateProductScrapInput";
import { ProductScrapService } from "@app/services/productScrap/productScrap.service";
import { MatDialogRef } from "@angular/material";
import { finalize } from "rxjs/operators";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ProductService } from "@app/services/product/product.service";
import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";
import { ScrapTypeService } from "@app/services/scrapType/scrapType.service";

@Component({
  selector: "app-create-productScrap-dialog",
  templateUrl: "./create-productScrap-dialog.component.html",
  styleUrls: ["./create-productScrap-dialog.component.css"],
})
export class CreateProductScrapDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  productScrap: CreateProductScrapInput = new CreateProductScrapInput();
  products: ProductFullOutPut[] = [];
  scrapTypes: ScrapTypeFullOutPut[] = [];

  constructor(
    injector: Injector,
    public _productScrapService: ProductScrapService,
    public _productService: ProductService,
    public _scrapTypeService: ScrapTypeService,
    private _dialogRef: MatDialogRef<CreateProductScrapDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getProducts();
    this.getScrapTypes();
  }

  getProducts(): ProductFullOutPut[] | any {
    this._productService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductFullOutPut[]) => {
        return (this.products = result);
      });
  }

  getScrapTypes(): ScrapTypeFullOutPut[] | any {
    this._scrapTypeService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ScrapTypeFullOutPut[]) => {
        return (this.scrapTypes = result);
      });
  }

  save(): void {
    this.saving = true;

    this._productScrapService
      .create(this.productScrap)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
