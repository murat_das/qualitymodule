import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ProductScrapFullOutPut } from "@app/services/productScrap/dtos/ProductScrapFullOutPut";
import { ProductScrapService } from "@app/services/productScrap/productScrap.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteProductScrapInput } from "@app/services/productScrap/dtos/DeleteProductScrapInput";

@Component({
  selector: "app-productScrap",
  templateUrl: "./productScrap.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./productScrap.component.css"],
  providers: [],
})
export class ProductScrapComponent extends AppComponentBase implements OnInit {
  productScraps: ProductScrapFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _productScrapService: ProductScrapService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createProductScrap(): void {
    this._modalManagerService
      .openCreateProductScrapDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editProductScrap(id: number): void {
    this._modalManagerService
      .openEditProductScrapDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteProductScrap(productScrap: DeleteProductScrapInput): void {
    abp.message.confirm(
      this.l(
        "ProductScrapDeleteWarningMessage",
        productScrap.product.name + "-" + productScrap.scrapType.name
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._productScrapService
            .delete(productScrap)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._productScrapService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ProductScrapFullOutPut[]) => {
                this.productScraps = result;
                resolve(this.productScraps);
              });
          });
        },
      }),
    });
  }
}
