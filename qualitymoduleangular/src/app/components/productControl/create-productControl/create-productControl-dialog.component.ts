import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateProductControlInput } from "@app/services/productControl/dtos/CreateProductControlInput";
import { ProductControlService } from "@app/services/productControl/productControl.service";
import { MatDialogRef } from "@angular/material";
import { finalize } from "rxjs/operators";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ProductService } from "@app/services/product/product.service";
import { ScrapTypeFullOutPut } from "@app/services/scrapType/dtos/ScrapTypeFullOutPut";
import { ScrapTypeService } from "@app/services/scrapType/scrapType.service";

@Component({
  selector: "app-create-productControl-dialog",
  templateUrl: "./create-productControl-dialog.component.html",
  styleUrls: ["./create-productControl-dialog.component.css"],
})
export class CreateProductControlDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  productControl: CreateProductControlInput = new CreateProductControlInput();
  products: ProductFullOutPut[] = [];

  constructor(
    injector: Injector,
    public _productControlService: ProductControlService,
    public _productService: ProductService,
    public _scrapTypeService: ScrapTypeService,
    private _dialogRef: MatDialogRef<CreateProductControlDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): ProductFullOutPut[] | any {
    this._productService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductFullOutPut[]) => {
        return (this.products = result);
      });
  }

  save(): void {
    this.saving = true;

    this._productControlService
      .create(this.productControl)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
