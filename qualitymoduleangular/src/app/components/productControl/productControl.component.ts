import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";
import { ProductControlService } from "@app/services/productControl/productControl.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteProductControlInput } from "@app/services/productControl/dtos/DeleteProductControlInput";

@Component({
  selector: "app-productControl",
  templateUrl: "./productControl.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./productControl.component.css"],
  providers: [],
})
export class ProductControlComponent extends AppComponentBase implements OnInit {
  productControls: ProductControlFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _productControlService: ProductControlService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createProductControl(): void {
    this._modalManagerService
      .openCreateProductControlDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editProductControl(id: number): void {
    this._modalManagerService
      .openEditProductControlDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteProductControl(productControl: DeleteProductControlInput): void {
    abp.message.confirm(
      this.l(
        "ProductControlDeleteWarningMessage",this.l("ProductControlAmount")+" "+productControl.amount+" - "+productControl.product.name
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._productControlService
            .delete(productControl)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._productControlService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ProductControlFullOutPut[]) => {
                this.productControls = result;
                resolve(this.productControls);
              });
          });
        },
      }),
    });
  }
}
