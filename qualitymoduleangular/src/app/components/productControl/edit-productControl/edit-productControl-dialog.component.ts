import { Component, OnInit, Injector, Optional, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/app-component-base";
import { ProductControlFullOutPut } from "@app/services/productControl/dtos/ProductControlFullOutPut";
import { ProductControlService } from "@app/services/productControl/productControl.service";
import { GetProductControlInput } from "@app/services/productControl/dtos/GetProductControlInput";
import { ProductFullOutPut } from "@app/services/product/dtos/ProductFullOutPut";
import { ProductService } from "@app/services/product/product.service";
import { ScrapTypeService } from "@app/services/scrapType/scrapType.service";

@Component({
  selector: "app-edit-productControl-dialog",
  templateUrl: "./edit-productControl-dialog.component.html",
  styleUrls: ["./edit-productControl-dialog.component.css"],
})
export class EditProductControlDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  productControl: ProductControlFullOutPut = new ProductControlFullOutPut();
  products: ProductFullOutPut[] = [];

  constructor(
    injector: Injector,
    public _productControlService: ProductControlService,
    public _productService: ProductService,
    public _scrapTypeService: ScrapTypeService,
    private _dialogRef: MatDialogRef<EditProductControlDialogComponent>,
    @Optional()
    @Inject(MAT_DIALOG_DATA)
    private _productControl: GetProductControlInput
  ) {
    super(injector);
  }

  ngOnInit() {
    this._productControlService
      .get(this._productControl)
      .subscribe((result: ProductControlFullOutPut) => {
        this.productControl = result;
      });

    this.getProducts();
  }

  getProducts(): ProductFullOutPut[] | any {
    this._productService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: ProductFullOutPut[]) => {
        return (this.products = result);
      });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._productControlService
      .update({
        id: this.productControl.id,
        product: this.productControl.product,
        amount: this.productControl.amount,
        seeded: this.productControl.seeded,
        workOrderId: 0,
      })
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        // toastr.options={
        //   "positionClass": "toast-top-right",
        // }
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  customCompare(o1, o2) {
    return o1.id === o2.id;
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
