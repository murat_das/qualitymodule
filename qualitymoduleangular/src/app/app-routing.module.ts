import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ProductComponent } from './components/product/product.component';
import { ScrapTypeComponent } from './components/scrapType/scrapType.component';
import { TestComponent } from './components/test/test.component';
import { UnitComponent } from './components/unit/unit.component';
import { ValueTypeComponent } from './components/valueType/valueType.component';
import { RuleComponent } from './components/rule/rule.component';
import { ProductTestComponent } from './components/productTest/productTest.component';
import { ProductScrapComponent } from './components/productScrap/productScrap.component';
import { ProductControlComponent } from './components/productControl/productControl.component';
import { ScrapComponent } from './components/scrap/scrap.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent,  canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'update-password', component: ChangePasswordComponent },
                    { path: 'products', component: ProductComponent, canActivate: [AppRouteGuard] },
                    { path: 'scrapTypes', component: ScrapTypeComponent, canActivate: [AppRouteGuard] },
                    { path: 'tests', component: TestComponent, canActivate: [AppRouteGuard] },
                    { path: 'units', component: UnitComponent, canActivate: [AppRouteGuard] },
                    { path: 'valueTypes', component: ValueTypeComponent, canActivate: [AppRouteGuard] },
                    { path: 'rules', component: RuleComponent, canActivate: [AppRouteGuard] },
                    { path: 'productTests', component: ProductTestComponent, canActivate: [AppRouteGuard] },
                    { path: 'productScraps', component: ProductScrapComponent, canActivate: [AppRouteGuard] },
                    { path: 'productControls', component: ProductControlComponent, canActivate: [AppRouteGuard] },
                    { path: 'scraps', component: ScrapComponent, canActivate: [AppRouteGuard] },



                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
