import { QualtyModuleTemplatePage } from './app.po';

describe('QualtyModule App', function() {
  let page: QualtyModuleTemplatePage;

  beforeEach(() => {
    page = new QualtyModuleTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
